# -*- coding: utf-8 -*- #
"""
Time                2023/09/19 10:30
Author:             chenshiyan
Email               mfeng@linux.alibaba.com
File                models.py
Description:
"""
from sqlalchemy import Column, Integer, String,DateTime
from app.database import Base
from sqlalchemy.dialects.mysql import LONGTEXT
###########################################################################
# Define databse model here
###########################################################################

class Knowledge(Base):
    __tablename__ = "knowledge"
    id = Column(Integer, primary_key=True)
    create_time = Column(DateTime)
    issuedesc = Column(LONGTEXT, default="")
    calltrace = Column(String(2560), default="")
    calltrace_extrack = Column(String(256), default="")
    logs = Column(String(2560), default="")
    funcs = Column(String(1280), default="")
    issuelink = Column(String(256), default="")
    field = Column(String(128), default="")
    diagret = Column(LONGTEXT, default="")

class KnowledgeRecord(Base):
    __tablename__ = "knowledge_record"
    id = Column(Integer, primary_key=True)
    recordtime = Column(DateTime)
    calltrace = Column(String(2560), default="")
    logs = Column(String(2560), default="")
    funcs = Column(String(1280), default="")
    source = Column(String(128), default="")
    knowledge_ret = Column(LONGTEXT, default="")

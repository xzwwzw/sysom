# -*- coding: utf-8 -*- #
"""
Time                2023/08/24 15:41
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                common.py
Description:
"""
from pathlib import Path
from sysom_utils import ConfigParser, SysomFramework, CecTarget

BASE_DIR = Path(__file__).resolve().parent.parent

##################################################################
# Load yaml config first
##################################################################
YAML_GLOBAL_CONFIG_PATH = f"{BASE_DIR.parent.parent}/conf/config.yml"
YAML_SERVICE_CONFIG_PATH = f"{BASE_DIR}/config.yml"

YAML_CONFIG = ConfigParser(YAML_GLOBAL_CONFIG_PATH, YAML_SERVICE_CONFIG_PATH)

cec_config = YAML_CONFIG.get_server_config().cec
mysql_config = YAML_CONFIG.get_server_config().db.mysql
service_config = YAML_CONFIG.get_service_config()

prometheus_config = YAML_CONFIG.get_server_config().db.prometheus

PROMETHEUS_URL = f"{prometheus_config.get('prototol', 'http')}://{prometheus_config.get('host', 'localhost')}:{prometheus_config.get('port', 9090)}"

SysomFramework.init(YAML_CONFIG)

##################################################################
# fastapi config
##################################################################
SQLALCHEMY_DATABASE_URL = (
    f"{mysql_config.dialect}+{mysql_config.engine}://{mysql_config.user}:{mysql_config.password}@"
    f"{mysql_config.host}:{mysql_config.port}/{mysql_config.database}"
)

##################################################################
# Cec settings
##################################################################
SYSOM_CEC_URL = YAML_CONFIG.get_cec_url(CecTarget.PRODUCER)
# 告警中心接收 SAD 格式告警的主题
CEC_TOPIC_SYSOM_SAD_ALERT = "SYSOM_SAD_ALERT"
# 告警中心接收对 SAD 格式告警数据进行修改的主题
CEC_TOPIC_SYSOM_ALARM_ACTION = "SYSOM_ALARM_ACTION"
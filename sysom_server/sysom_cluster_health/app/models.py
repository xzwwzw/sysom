# -*- coding: utf-8 -*- #
"""
Time                2023/11/29 10:08
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                models.py
Description:
"""
from sqlalchemy import Column, Integer, String, DateTime, Float
from sqlalchemy.sql import func
from app.database import Base


###########################################################################
# Define databse model here
###########################################################################

class BaseModel:
    uuid = Column(String(128), primary_key=True, unique=True)
    metric_id = Column(String(256))
    metric_type = Column(String(128))
    score = Column(Float)
    value = Column(Float)
    timestamp = Column(Float, default=func.time())
    
class AbnormalMetricsCluster(Base, BaseModel):
    __tablename__ = "sys_abnormal_metrics_cluster"

    cluster = Column(String(256))
    
class AbnormalMetricsNode(Base, BaseModel):
    __tablename__ = "sys_abnormal_metrics_node"

    cluster = Column(String(256))
    instance = Column(String(256))
    
class AbnormalMetricsPod(Base, BaseModel):
    __tablename__ = "sys_abnormal_metrics_pod"

    cluster = Column(String(256))
    instance = Column(String(256))
    pod = Column(String(256))
    namespace = Column(String(256))
    

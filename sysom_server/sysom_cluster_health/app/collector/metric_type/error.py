from lib.common_type import Level
from app.collector.metric_type.metric_type import Metric, MetricReader,\
    RangeAggregationType, InsAggregationType
from app.collector.metric_exception import MetricSettingsException


class ErrorMetric(Metric):
    def __init__(self, metric_reader: MetricReader, metric_settings,
                 level: Level):
        super().__init__(metric_reader, metric_settings, level)

    def _collect_process_metric(self) -> float:
        if self.settings.collect.standard_type != 1:
            raise MetricSettingsException()

        return super()._default_single_counter(
            related_value=self.settings.collect.related_value[0],
            range_agg_type=RangeAggregationType.Increase,
            ins_agg_type=InsAggregationType.Sum
        )

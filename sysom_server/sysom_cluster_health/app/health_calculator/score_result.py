from dataclasses import dataclass
from enum import Enum
from typing import Dict, NewType, List

class ScoreType(Enum):
    MetricScore = 1
    InstanceScore = 2
    MetricTypeScore = 3


class ScoreResult:
    def __init__(self, labels: Dict[str, str], score: float,
                 value: float, type: ScoreType):
        self.labels = labels
        self.score = score
        self.value = value
        self.type = type

    def keys(self):
        return ('labels', 'score', 'value', 'type')

    def __getitem__(self, item):
        return getattr(self, item)

    def to_dict(self):
        result_dict = {}
        for key in self.keys():
            value = self[key]
            if isinstance(value, ScoreType):
                result_dict[key] = value.value
            else:
                result_dict[key] = value
        return result_dict


# [metric_type: [metrics'score result]]
TypeResult = NewType('TypeResult', Dict[str, List[ScoreResult]])


@dataclass
class LevelResults:
    labels: Dict[str, str]
    results: TypeResult
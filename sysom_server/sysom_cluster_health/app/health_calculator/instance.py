import json
import time
from conf.settings import *
from typing import Dict, List
from datetime import datetime
from clogger import logger
from sysom_utils import SysomFramework
from lib.utils import collect_all_clusters, \
    collect_instances_of_cluster, collect_pods_of_instance
from app.health_calculator.health_metric import HealthMetric, HealthMetricsMap
from app.health_calculator.score_result import ScoreResult, ScoreType
from app.schemas import AbnormalMetricsBase
from app.crud import create_abnormal_metrics_data
from app.database import SessionLocal
from abc import ABC, abstractmethod

gcache_cluster_exporter = SysomFramework.gcache(CLUSTER_METRIC_EXPORTER)
gcache_node_exporter = SysomFramework.gcache(NODE_METRIC_EXPORTER)
gcache_pod_exporter = SysomFramework.gcache(POD_METRIC_EXPORTER)

class Instance(ABC):
    def __init__(self, name: str):
        self.name = name
        self.metrics = {}   # type: Dict[str, HealthMetric]  
        self.type_score = {"capacity": 100, "load": 100, "latency": 100, "error": 100}
        self.score = 100
    
    
    def _push_score_result(self):
        """
        Insert the health score result to gcache
        
        args:
            result: the health score result
        """
        raise NotImplementedError("Must implement _push_score_result")
        
    def _insert_score_result(
        self,
        level_labels: Dict[str, str], 
        abnormal_metrics: Dict[str, HealthMetricsMap]
    ) -> List[dict]:
        score_result = []
        for type, metrics_map in abnormal_metrics.items():
            # store abnormal metrics to as prometheus metrics
            if ABNORMAL_METRIC_STORAGE == "prometheus":
                for abnormal_metric in metrics_map.metrics_map.values():
                    labels = level_labels.copy()
                    labels["description"] = abnormal_metric.metric_id
                    labels["type"] = type
                    score_result.append(
                        ScoreResult(
                            labels, abnormal_metric.score, 
                            abnormal_metric.value, ScoreType.MetricScore
                        ).to_dict()
                    )
            # store abnormal metrics to as mysql data
            elif ABNORMAL_METRIC_STORAGE == "mysql":
                with SessionLocal() as db:
                    for metric_id, abnormal_metric in metrics_map.metrics_map.items():
                        abnormal_metric_data = AbnormalMetricsBase(
                            metric_id=metric_id,
                            metric_type=type,
                            cluster=level_labels["cluster"],
                            instance=level_labels.get("instance", ""),
                            namespace=level_labels.get("namespace", ""),
                            pod=level_labels.get("pod", ""),
                            score=abnormal_metric.score,
                            value=abnormal_metric.value,
                            timestamp=time.time()
                        )
                        create_abnormal_metrics_data(db, abnormal_metric_data)

        for type, score in self.type_score.items():
            labels = level_labels.copy()
            labels["type"] = type
            score_result.append(
                ScoreResult(
                    labels, score, 0, ScoreType.MetricTypeScore
                ).to_dict()
            )
        
        score_result.append(
            ScoreResult(
                level_labels, self.score, 0, ScoreType.InstanceScore
            ).to_dict()
        )
        
        return score_result
    
    def _validate_metric_time(self, metric: HealthMetric):
        now = time.time()
        if (now - metric.event_time) > 2 * CALCULATE_INTERVAL:
            event_time = datetime.fromtimestamp(metric.event_time)
            now_datetime = datetime.fromtimestamp(now)
            logger.warning(f"Metric {metric.metric_id} is too old, "
                           f"event_time: {event_time} now: {now_datetime}")
            return False

        return True
    
    def _add_metric(self, metric: HealthMetric):
        if not self._validate_metric_time(metric):
            return
        
        metric_id = metric.metric_id
        # multiple metrics with the same metric_id, use the worst one
        if metric_id in self.metrics:
            if metric.score >= self.metrics[metric_id].score:
                return
                     
        self.metrics[metric.metric_id] = metric
        
    def _collect_metrics_from_gcacge(self, key, gcache):
        metrics_list = gcache.get_list(key)
        for metric_data in metrics_list:
            health_metric = HealthMetric(**metric_data)
            self._add_metric(health_metric)

    def _lower_level_instances_score(self) -> List[float]:
        return []
              
    def calculate_health(self, algorithm):
        try:
            algorithm.preprocessing(self.metrics)
            
            (
                self.type_score["capacity"],
                self.type_score["load"],
                self.type_score["latency"],
                self.type_score["error"],
                this_level_score
            ) = algorithm.calculate_this_level()
            
            lower_instances_score = self._lower_level_instances_score()
            lower_level_score = algorithm.calculate_lower_level(
                lower_instances_score
            )
            
            abnormal_metrics = algorithm.get_abnormal_metrics()
        except Exception as e:
            logger.error(f"Calculate {self.name} health failed: {e}")
            # set score to -1 to indicate the health score is invalid
            self.score = -1
            return

        self.score = min(lower_level_score, this_level_score)
        self._push_score_result(abnormal_metrics)
    
    @abstractmethod
    def collect_metrics(self, gcache):
        pass
    

# Container Level is not implemented yet
class Container(Instance):
    def __init__(self, name: str, pod: Instance):
        self.pod = pod
        super().__init__(name)
        
class Pod(Instance):
    def __init__(self, name: str, namespace: str, node: Instance):
        self.node = node
        self.namespace = namespace
        self.containers = {}
        super().__init__(name)
        
    def _push_score_result(self, abnormal_metrics: Dict[str, HealthMetricsMap]):
        level_labels = {
            "cluster": self.node.cluster.name,
            "instance": self.node.name,
            "pod": self.name,
            "namespace": self.namespace,
        }

        score_result = self._insert_score_result(level_labels, abnormal_metrics)
        gcache_pod_exporter.store(self.name, json.dumps(score_result))
        
    def collect_metrics(self, gcache):
        key = self.node.cluster.name + ":" + self.name + ":" + self.namespace
        self._collect_metrics_from_gcacge(key, gcache)
        
    def _lower_level_instances_score(self) -> List[float]:
        return super()._lower_level_instances_score()
    
    def add_container(self, container: Container):
        self.containers[container.name] = container


class Node(Instance):
    def __init__(self, name: str, cluster: Instance):
        self.pods = {}
        self.cluster = cluster
        super().__init__(name)
        
    def add_pod(self, pod: Pod):
        self.pods[pod.name] = pod
        
    def find_pod(self, pod_name: str) -> Pod:
        return self.pods[pod_name]
    
    def _lower_level_instances_score(self) -> List[float]:
        return [
            pod.score
            for pod in self.pods.values()
            if 0 <= pod.score <= 100
        ]
    
    def _push_score_result(self, abnormal_metrics: Dict[str, HealthMetricsMap]):
        level_labels = {
            "cluster": self.cluster.name,
            "instance": self.name,
        }
          
        score_result = self._insert_score_result(level_labels, abnormal_metrics)
        gcache_node_exporter.store(self.name, json.dumps(score_result))
        
    def collect_metrics(self, gcache):
        key = self.cluster.name + ":" + self.name
        self._collect_metrics_from_gcacge(key, gcache)
        

class Cluster(Instance):
    def __init__(self, name: str):
        self.nodes = {}
        super().__init__(name)

    def add_node(self, node: Node):
        self.nodes[node.name] = node
        
    def find_node(self, node_name: str) -> Node:
        return self.nodes[node_name]
    
    def _lower_level_instances_score(self) -> List[float]:
        return [
            node.score
            for node in self.nodes.values()
            if 0 <= node.score <= 100
        ]
    
    def _lower_level_type_score(self, type: str) -> List[float]:
        return [
            node.type_score[type] 
            for node in self.nodes.values() 
            if 0 <= node.type_score[type] <= 100
        ]
    
    def _push_score_result(self, abnormal_metrics: Dict[str, HealthMetricsMap]):
        level_labels = {
            "cluster": self.name,
        }
          
        score_result = self._insert_score_result(level_labels, abnormal_metrics)
        gcache_cluster_exporter.store(self.name, json.dumps(score_result))
        
    def collect_metrics(self, gcache):
        key = self.name
        self._collect_metrics_from_gcacge(key, gcache)
        
    def calculate_health(self, algorithm):
        try:
            for type in self.type_score.keys():
                self.type_score[type] = algorithm.calculate_lower_level(
                    self._lower_level_type_score(type)
                )
            
            self.score = algorithm.calculate_lower_level(
                self._lower_level_instances_score()
            )
        except Exception as e:
            logger.error(f"Calculate {self.name} health failed: {e}")
            # set score to -1 to indicate the health score is invalid
            self.score = -1
            return
        
        self._push_score_result({})
           

def construct_cluster_infos(metric_reader, interval) -> Dict[str, Cluster]:
    """ 
    Construct cluster infos from prometheus metrics
    """
    res = {}
    clusters = collect_all_clusters(metric_reader)
    if len(clusters) == 0 or NO_CLUSTER_LABEL is True:
        clusters.append("default")
    
    for cluster in clusters:
        cluster_instance = Cluster(cluster)
        nodes = collect_instances_of_cluster(cluster, 
                                             metric_reader, interval)
        for node in nodes:
            node_instance = Node(node, cluster_instance)
            pods = collect_pods_of_instance(node, 
                                            metric_reader, interval)
            for pod, ns in pods:
                pod_instance = Pod(pod, ns, node_instance)
                node_instance.add_pod(pod_instance)
            cluster_instance.add_node(node_instance)

        res[cluster] = cluster_instance

    return res

        
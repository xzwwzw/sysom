import json
import conf.settings as settings
from abc import abstractmethod
from typing import List, Dict, Tuple
from clogger import logger
from lib.common_type import Level
from app.health_calculator.health_metric import HealthMetric, HealthMetricsMap

METRIC_TYPES = ["capacity", "load", "latency", "error"]

class HealthAlgorithm:
    def __init__(self, level: Level):
        self.level = level
        self.registed_metric = {} # metric_id -> metric_setting map
        self.type_metrics = {
            metric_type: [] for metric_type in METRIC_TYPES
        } # type -> [metric_setting] map
        self.output_abnormal_metrics = {
            metric_type: HealthMetricsMap() for metric_type in METRIC_TYPES
        }

    @abstractmethod
    def preprocessing(self, metrics: Dict[str, HealthMetric]):
        """
        Preprocess the data
        
        args:
            metrics: the metrics data receive from gcache metric set
            metrics = {
                "metric_id1": HealthMetric
                "metric_id2": HealthMetric
                ...
            }

        """
        raise NotImplementedError("Subclass must implement abstract method")

    @abstractmethod
    def calculate_this_level(self) -> Tuple[float, float, float, float, float]:
        """
        The method to calculate the health of this level
        
        return: (capacity_score, load_score, latency_score, error_score, instance_score)
        """
        raise NotImplementedError("Subclass must implement abstract method")
    
    
    def calculate_lower_level(self, values: List[float]) -> float:
        """
        The method to calculate the lower level health to this level
        
        args:
            values: the health score of lower level instances
            assume we are calculating a node's score, the values = 
            [pod1_score, pod2_score, pod3_score3]
            
        return: the health score of this instance(cluster, node, pod)
        """
        if len(values) <= 0:
            return 100
        
        bad_instances = []
        for value in values:
            if value < 0 or value > 100:
                raise ValueError(f"Score: {value} is invalid")
            if value < 60:
                bad_instances.append(value)
        
        # no bad instances    
        if len(bad_instances) <= 0:
            return sum(values) / len(values)
        
        if len(bad_instances) == len(values):
            return 0
        
        bad_instances_ratio = len(bad_instances) / len(values)
        # if the ratio of bad instances is more than 10%, the health score will be under 60
        if bad_instances_ratio >= 0.1:
            return 60 * (1 - (len(bad_instances) / len(values) - 0.1))
        if bad_instances_ratio < 0.1:
            return 90 - 30 * (len(bad_instances) / (len(values) * 0.1))
        
        return 100


    def register_metric_from_settings(self):
        metrics_mapping = {
            Level.Cluster: settings.CLUSTER_HEALTH_METRICS,
            Level.Node: settings.NODE_HEALTH_METRICS,
            Level.Pod: settings.POD_HEALTH_METRICS
        }

        metrics = metrics_mapping.get(self.level)
        if metrics is not None:
            for metric in metrics:
                if "MetricID" not in metric or "Type" not in metric:
                    logger.warning(f"Setting: metric {json.dumps(metric)}"
                                   f"is invalid, skip it")
                    continue
            
                metric_id = metric["MetricID"]
                metric_type = metric["Type"]
                
                if metric_type not in METRIC_TYPES:
                    logger.warning(f"Setting: metric {metric_id}"
                                   f"type {metric_type} is invalid, skip it")
                    continue
    
                self.registed_metric[metric_id] = metric
                self.type_metrics[metric_type].append(metric)
                
    
    def get_abnormal_metrics(self) -> Dict[str, HealthMetricsMap]:
        return self.output_abnormal_metrics


class DefaultHealthAlgorithm(HealthAlgorithm):
    def __init__(self, level: Level):
        super().__init__(level)
    
    def preprocessing(self, metrics: Dict[str, HealthMetric]):
        categories = ["critical_abnormal", "suspect_abnormal", "trend_abnormal"]
        types = METRIC_TYPES + ["instance"]
        score_ranges = [(0, 60), (60, 90), (90, 100)]

        self.data = {type: {category: [] for category in categories} for type in types}

        def __add_metric_to_data(metric, category):
            self.data[metric_type][category].append(metric)
            self.data["instance"][category].append(metric)
            self.output_abnormal_metrics[metric_type].add_metric(metric)

        for metric in metrics.values():
            if metric.metric_id not in self.registed_metric:
                logger.warning(f"Receive metric {metric.metric_id} not in registed metrics")
                continue

            metric_type = self.registed_metric[metric.metric_id]["Type"]

            score = metric.score
            if score < 0 or score > 100:
                logger.warning(f"Metric {metric.metric_id} score {score} is invalid")
                continue

            for i, (low, high) in enumerate(score_ranges):
                if low <= score < high:
                    __add_metric_to_data(metric, categories[i])
                    break
        

    def calculate_this_level(self) -> Tuple[float, float, float, float, float]:
        def _calculate_score(abnormal_dict, metric_num) -> float:
            if metric_num <= 0:
                return 100
            
            if len(abnormal_dict["critical_abnormal"]) > 0:
                return 60 * (1 - len(abnormal_dict["critical_abnormal"]) / metric_num)
            
            if len(abnormal_dict["suspect_abnormal"]) > 0:
                return 90 - 30 * (len(abnormal_dict["suspect_abnormal"]) / metric_num)
            
            if len(abnormal_dict["trend_abnormal"]) > 0:
                return 100 - 10 * (len(abnormal_dict["trend_abnormal"]) / metric_num)
            
            return 100
        
        res = []
        for type in METRIC_TYPES:
            type_registed_metrics = self.type_metrics.get(type, {})
            res.append(_calculate_score(self.data[type], len(type_registed_metrics)))

        res.append(_calculate_score(self.data["instance"], len(self.registed_metric)))
        return tuple(res)
        
    
class WeightedSumAlgorithm(HealthAlgorithm):
    def __init__(self, level: Level):
        super().__init__(level)

        
    def register_metric_from_settings(self):               
        def __check_weight(metrics):
            for type, metrics in metrics.items():
                if len(metrics) <= 0:
                    continue

                weight = 0
                for metric in metrics:
                    weight += metric["Weight"]
                if weight != 1:
                    raise Exception(f"Sum of weight of {type} metrics of "
                                    f"level {self.level} is not equal 1")
                    
        super().register_metric_from_settings()
        __check_weight(self.type_metrics)


    def preprocessing(self, metrics: Dict[str, HealthMetric]):
        self.data = {
            metric_type: {} for metric_type in METRIC_TYPES
        }
        for metric in metrics.values():
            if metric.metric_id not in self.registed_metric:
                logger.warning(f"Receive metric {metric.metric_id} not in registed metrics")
                continue
            
            metric_type = self.registed_metric[metric.metric_id]["Type"]
            self.data[metric_type][metric.metric_id] = metric


    def calculate_this_level(self) -> Tuple[float, float, float, float, float]:
        """
        Calculate the health score of this level using weighted sum algorithm:
        type_score = sum(metric_score * metric_weight)
        instance_score = avg(type_score)
        """
        
        res = []
        for type, type_metrics in self.type_metrics.items():
            if len(type_metrics) <= 0:
                res.append(100)
                continue
            
            type_score = 0
            if type not in self.data:
                logger.warning("WeightedSumAlgorithm: Type {type} not in receive data")
                type_score = 100
                res.append(type_score)
                continue

            for metric in type_metrics:
                metric_score = 100
                metric_id = metric["MetricID"]
                if metric_id not in self.data[type]:
                    logger.warning(f"WeightedSumAlgorithm: Metric {metric} not in data")
                else:
                    metric_score = self.data[type][metric_id].score
                
                if metric_score < 100:
                    self.output_abnormal_metrics[type].add_metric(self.data[type][metric_id])

                type_score += metric_score * metric["Weight"]
            
            res.append(type_score)
        
        instance_score = sum(res) / len(res)
        res.append(instance_score)
        print("WeightedSumAlgorithm: calculated score: ", res)
        return tuple(res)
        
         
class EwmAlgorithm(HealthAlgorithm):
    def __init__(self, level: Level):
        super().__init__(level)
    
    def preprocessing(self, metrics: Dict[str, HealthMetric]):
        pass

    def calculate_this_level(self) -> Tuple[float, float, float, float, float]:
        pass
    
class CriticAlgorithm(HealthAlgorithm):
    def __init__(self, level: Level):
        super().__init__(level)
    
    def preprocessing(self, metrics: Dict[str, HealthMetric]):
        pass

    def calculate_this_level(self) -> Tuple[float, float, float, float, float]:
        pass

def choose_algorithm(alg_setting: str, level: Level) -> HealthAlgorithm:
    algorithm_mapping = {
        "default": DefaultHealthAlgorithm,
        "weightedSum": WeightedSumAlgorithm,
        "ewm": EwmAlgorithm,
        "critic": CriticAlgorithm
    }
    
    algorithm_class = algorithm_mapping.get(alg_setting, DefaultHealthAlgorithm)
    algorithm_instance = algorithm_class(level)

    try:
        algorithm_instance.register_metric_from_settings()
    except Exception as e:
        logger.error(f"Algorithm {alg_setting} init failed: {e}")
        raise e
    
    logger.info(f"选择算法: {algorithm_class.__name__}，层级: {level}")
    
    return algorithm_instance


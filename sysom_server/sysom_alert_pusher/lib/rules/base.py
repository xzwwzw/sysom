from abc import ABCMeta, abstractmethod
from app.schemas import AlertData
from typing import List

class PushRuleBase(metaclass=ABCMeta):
    def __init__(self, config: dict) -> None:
        self.config = config
        self.targets = config.get("targets", [])
        
    @abstractmethod
    def is_match(self, data: dict) -> bool:
        """Return whether the rule is matched.

        Args:
            data (dict): _description_
        """
        pass
    
    def is_match_alert_data(self, alert_data: AlertData) -> bool:
        """Return whether the rule is matched.

        Args:
            alert_data (AlertData): _description_
        """
        return self.is_match(alert_data.dict())
    
    def get_targets(self) -> List[str]:
        return self.targets
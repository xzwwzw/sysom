# -*- coding: utf-8 -*- #
"""
Time                2023/08/02 14:32
Author:             shiyan
Email               chenshiyan@linux.alibaba.com
File                models.py
Description:
"""
from sqlalchemy import Column, Integer, String
from app.database import Base
from sqlalchemy.dialects.mysql import LONGTEXT

###########################################################################
# Define databse model here
###########################################################################

class RcaCallRecord(Base):
    __tablename__ = "rcacall_record"
    id = Column(Integer, primary_key=True)
    recordid = Column(String(254), unique=True)
    state = Column(String(64), default="")
    url = Column(String(256), default="")
    user = Column(String(128), default="")
    timestamp = Column(Integer, default=0)

class RcaItemsRecord(Base):
    __tablename__ = "rcaitems_record"
    id = Column(Integer, primary_key=True)
    recordid = Column(String(254), unique=True)
    machine_ip = Column(String(64), default="")
    time_occur = Column(Integer, default=0)
    time_start = Column(Integer, default=0)
    time_end = Column(Integer, default=0)
    state = Column(String(64), default="")
    url = Column(String(256), default="")
    user = Column(String(128), default="")
    metric_dict = Column(LONGTEXT, default="")
    rca_conclusion = Column(LONGTEXT, default="")
    final_conclusion = Column(LONGTEXT, default="")

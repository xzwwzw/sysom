import time
import conf.settings as settings
from multiprocessing import Process
from schedule import Scheduler
from os import getpid
from conf.common import PROMETHEUS_DATABASE_URL
from sysom_utils import SysomFramework
from clogger import logger
from metric_reader import dispatch_metric_reader
from lib.utils import (
    collect_all_clusters,
    collect_instances_of_cluster,
    generate_unique_key,
)
from .instance_manager import InstanceManager


class PeriodPredictWorker(Process):
    def __init__(self, interval_sec: int = 15) -> None:
        super().__init__(daemon=True)
        self.interval_sec = interval_sec
        self.scheduler: Scheduler = Scheduler()
        self.current_pid = getpid()
        self.metric_reader = dispatch_metric_reader(PROMETHEUS_DATABASE_URL)
        self.instances = {}

    def _check_instance(self) -> None:
        cluster_list = collect_all_clusters(self.metric_reader)

        # no cluster label, we assume just one, and names it "dafault"
        if len(cluster_list) == 0 or settings.NO_CLUSTER_LABEL:
            cluster_list.append("default")

        for cluster in cluster_list:
            instance_list = collect_instances_of_cluster(
                cluster, self.metric_reader, 60
            )
            for instance in instance_list:
                unique_key = generate_unique_key(cluster, instance)
                if unique_key in self.instances.keys():
                    continue
                logger.info(
                    f"create new InstanceManager cluster is {cluster} instance is {instance}"
                )
                self.instances[unique_key] = InstanceManager(
                    cluster,
                    instance,
                    self.metric_reader,
                    SysomFramework.gcache(settings.CACHE_RT),
                    SysomFramework.gcache(settings.CACHE_FUTURE),
                )

    def _update(self) -> None:
        self._check_instance()
        for ins in self.instances.values():
            ins.call()

    def run(self) -> None:
        logger.info(f"PeriodModleWorker running on pid {self.current_pid}")

        self._update()
        self.scheduler.every(self.interval_sec).seconds.do(self._update)

        while True:
            if self.is_alive():
                try:
                    self.scheduler.run_pending()
                except Exception as e:
                    logger.error(f"PeriodModleWorker error {e}")
                finally:
                    time.sleep(max(1, int(self.interval_sec / 2)))
            else:
                break

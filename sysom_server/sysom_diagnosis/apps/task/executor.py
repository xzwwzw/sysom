import time
import base64
from clogger import logger
from typing import Optional
from schedule import Scheduler
from apps.task.models import JobModel
from django.conf import settings
from cec_base.event import Event
from cec_base.consumer import Consumer
from cec_base.cec_client import StoppableThread
from sysom_utils import AsyncEventExecutor, CecAsyncConsumeTask, ConfigParser
from asgiref.sync import sync_to_async
from datetime import datetime, timedelta
from django.db.models import Q
from lib.utils import uuid_8
from .helper import DiagnosisHelper, DiagnosisTaskResult, DiagnosisJobResult
from service_scripts.base import DiagnosisJob


class DiagnosisTaskExecutor(AsyncEventExecutor):
    def __init__(self, config: ConfigParser):
        super().__init__(settings.SYSOM_CEC_PRODUCER_URL, callback=self.process_event)
        self._check_task_schedule: Scheduler = Scheduler()
        self._check_task_process_thread: Optional[StoppableThread] = None
        self._check_interval: int = settings.CHECK_INTERVAL
        self._task_execute_timeout: int = settings.TASK_EXECUTE_TIMEOUT
        self.append_group_consume_task(
            settings.SYSOM_CEC_DIAGNOSIS_TASK_DISPATCH_TOPIC,
            settings.SYSOM_CEC_DIAGNOSIS_CONSUMER_GROUP,
            Consumer.generate_consumer_id(),
            ensure_topic_exist=True,
        )
        self.append_group_consume_task(
            settings.SYSOM_CEC_OFFLINE_ORIGIN_DIAGNOSIS_RESULT_TOPIC,
            settings.SYSOM_CEC_DIAGNOSIS_CONSUMER_GROUP,
            Consumer.generate_consumer_id(),
            ensure_topic_exist=True,
        )

    async def process_event(self, event: Event, task: CecAsyncConsumeTask):
        try:
            if task.topic_name == settings.SYSOM_CEC_DIAGNOSIS_TASK_DISPATCH_TOPIC:
                await self._process_task_dispatch_event(event)
            elif (
                task.topic_name
                == settings.SYSOM_CEC_OFFLINE_ORIGIN_DIAGNOSIS_RESULT_TOPIC
            ):
                await self._process_offline_origin_diagnosis_result_event(event)
            else:
                # Unexpected
                logger.error("Receive unknown topic event, unexpected!!")
        except Exception as exc:
            logger.exception(exc)
        finally:
            task.ack(event)

    ################################################################################################
    # 事件处理
    ################################################################################################
    async def _process_task_dispatch_event(self, event: Event):
        """Process diagnosis task dispatch event
        {
            "task_id": "xxx"
        }
        """
        try:
            if isinstance(event.value, dict):
                await self._execute_diagnosis_task_by_id(event.value["task_id"])
            else:
                raise Exception("expect event.value is dict")
        except Exception as exc:
            logger.exception(f"Diagnosis process task dispatch event error: {str(exc)}")

    async def _process_offline_origin_diagnosis_result_event(self, event: Event):
        """Process offline full diagnosis result event
        {
            # 诊断类型
            "service_name":  "xxx",

            # 诊断ID，如果有的话
            "task_id":  "",

            # 诊断参数，如果有的话
            "params":  {
                "instance": "xxx"
            },

            # 诊断命令
            "command": {
                "in_order": false,
                "jobs": [
                    {
                        "instance": "172.17.0.1",
                        "cmd": "sysak ossre_client -s > /dev/null && cat /var/log/sysak/ossre.log"
                    }
                ]
            }

            # 结果内容编码方式（同离线诊断回传接口）
            "content_encoding":  "xxx",

            # 结果内容（一个job对应一个结果，如果前处理脚本只返回一个job，则回传结果列表里面只包含一个字符串代表结果即可，同离线诊断回传接口）
            "results": [],

            "created_by": "xxx"
        }
        """
        try:
            assert isinstance(event.value, dict)
            service_name = event.value.get("service_name", "")
            task_id = event.value.get("task_id", uuid_8())
            params = event.value.get("params", {})
            if "channel" not in params:
                params["channel"] = "offline"
            content_encoding = event.value.get("content_encoding", "text")
            results = event.value.get("results", [])
            command = event.value.get(
                "command",
                {
                    "in_order": False,
                    "jobs": [
                        {"instance": params.get("instance", ""), "cmd": ""}
                        for _ in results
                    ],
                },
            )
            created_by = event.value.get("created_by", "cec")

            if content_encoding == "base64":
                results = [base64.b64decode(result).decode() for result in results]

            # 1. Get task
            instance = None
            try:
                instance = await sync_to_async(JobModel.objects.get)(task_id=task_id)
                if instance.status not in ["Ready", "Running"]:
                    return
            except JobModel.DoesNotExist:
                pass
            if instance is None:
                task_params = {
                    "task_id": task_id,
                    "command": command,
                    "created_by": created_by,
                    "params": params,
                    "service_name": service_name,
                    "status": "Running",
                }
                instance = await sync_to_async(JobModel.objects.create)(**task_params)

            # 2. Build diagnosis task result
            commands = instance.command.get("jobs", [])
            job_result = DiagnosisTaskResult(
                0,
                job_results=[
                    DiagnosisJobResult(
                        0,
                        stdout=result,
                        job=DiagnosisJob.from_dict(commands[idx]),
                        file_list=[],
                    )
                    for idx, result in enumerate(results)
                ],
                in_order=False,
            )

            # 3. Invoke postprocess script
            await DiagnosisHelper.postprocess_async(instance, job_result)

        except Exception as exc:
            logger.exception(
                f"Diagnosis process offline full diagnosis result event error: {str(exc)}"
            )

    ################################################################################################
    # 诊断任务执行
    ################################################################################################

    async def _execute_diagnosis_task_by_id(self, task_id: str):
        instance = await sync_to_async(JobModel.objects.get)(task_id=task_id)
        await self._execute_diagnosis_task_by_model(instance)

    async def _execute_diagnosis_task_by_model(self, instance: JobModel):
        # 1. Preprocess
        channel = instance.params.get("channel", "")
        ignore_channels = settings.IGNORE_TOOL_CHECK_CHANNELS
        res = await DiagnosisHelper.preprocess_async(
            instance, channel in ignore_channels
        )

        if not res:
            raise Exception("Diagnosis preprocess error, DiagnosisTask is None")

        # 1.1 Preprocess post wrapper
        is_offline = await DiagnosisHelper.preprocess_post_wrapper_async(instance, res)
        await DiagnosisHelper._update_job_async(instance, status="Running")
        if is_offline:
            return

        # 2. Execute and Postprocess
        if not res.offline_mode:
            job_result = await DiagnosisHelper.execute_async(instance, res)
        else:
            job_result = DiagnosisTaskResult(
                0,
                job_results=[
                    DiagnosisJobResult(
                        0,
                        stdout=item,
                        job=res.jobs[idx] if len(res.jobs) > idx else None,
                    )
                    for idx, item in enumerate(res.offline_results)
                ],
                in_order=res.in_order,
            )
        await DiagnosisHelper.postprocess_async(instance, job_result)

        # 3. TODO: produce task execute result to cec

    ################################################################################################
    # 轮询检查任务是否超时
    ################################################################################################
    def _check_task_timeout(self):
        # Check and mark timeout tasks
        expire_minutes_ago = datetime.now() - timedelta(
            minutes=self._task_execute_timeout
        )
        instances = JobModel.objects.filter(
            Q(created_at__lte=expire_minutes_ago) & (Q(status__in=["Running", "Ready"]))
        )
        for instance in instances:
            instance.code = 1
            instance.status = "Fail"
            instance.result = "Diagnosis execute task timeout"
            instance.err_msg = "Diagnosis execute task timeout"
            instance.save()

    def _check_task_thead(self):
        """check channel job thead schedule"""
        self._check_task_schedule.every(self._check_interval).seconds.do(
            self._check_task_timeout
        )

        while True:
            if (
                not self._check_task_process_thread.stopped()
                and self._check_task_process_thread.is_alive()
            ):
                self._check_task_schedule.run_pending()
                time.sleep(self._check_interval * 0.8)

    def start(self):
        super().start()

        if (
            self._check_task_process_thread is not None
            and not self._check_task_process_thread.stopped()
            and self._check_task_process_thread.is_alive()
        ):
            return

        self._check_task_process_thread = StoppableThread(target=self._check_task_thead)
        self._check_task_process_thread.setDaemon(True)
        self._check_task_process_thread.start()

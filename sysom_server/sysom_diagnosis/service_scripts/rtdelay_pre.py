"""
Time                2023/07/25
Author:             dongyunyao (zhiying)
File                rtdelay_pre.py
Description:
"""
from .base import DiagnosisJob, DiagnosisPreProcessor, DiagnosisTask


class PreProcessor(DiagnosisPreProcessor):
    """Command diagnosis

    Just invoke command in target instance and get stdout result

    Args:
        DiagnosisPreProcessor (_type_): _description_
    """

    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        pid = params.get("pid", "-1")
        server_pid = params.get("server_pid", "-1")
        instance = params.get("instance", "")
        lasting_time = params.get("time", "")
        if not lasting_time.strip():
            lasting_time = "30"
        command = "sysak -g rtdelay -d "+ lasting_time +" -p " + pid
        if server_pid!="-1":
            command += " -s " + server_pid
        return DiagnosisTask(
            jobs=[
                DiagnosisJob(instance=instance, cmd=command)
            ],
            in_order=False,
        )

from datetime import datetime
import json
import traceback
from typing import List
from uuid import uuid4
from clogger import logger
from metric_reader.metric_reader import dispatch_metric_reader
from metric_reader.task import InstantQueryTask
from .base import DiagnosisJob, DiagnosisPreProcessor, DiagnosisTask

# TODO data query use exported instance, only contain host, so if occur one host multi prometheus client will be wrong


def validate_time(time_str: str) -> datetime:
    try:
        return datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
    except ValueError:
        raise Exception(f"Time format error. time_str={time_str}")


class ServeUtilDiagnose:
    def __init__(self, instance: str, dt: datetime):
        self._instance = instance
        self._dt = dt
        self._ts = dt.timestamp()
        self._metric_reader = dispatch_metric_reader("prometheus://localhost:9090")
        self._container_table = []
        self._internal_err = 0

        self._status = "N/A"
        self._cause = "N/A"
        self._suggestion = "N/A"

    def _query_all_container(self) -> List[dict]:
        task = (
            InstantQueryTask("sysom_container_cfs_statis", self._dt.timestamp())
            .append_equal_filter("exported_instance", self._instance)
            .append_equal_filter("bvt", "LS")
            .append_wildcard_filter("value", "qother|qslibling|serveutil")
        )
        result = self._metric_reader.instant_query([task])
        if result.code != 0:
            raise Exception(
                f"Query sysom_container_cfs_statis all container data filed. code={result.code} err_msg={result.err_msg}"
            )
        container_list = {}
        for item in result.data:
            lables = item.labels
            key = f"{lables['namespace']}-{lables['pod']}-{lables['container']}"
            data = item.value

            if lables["container"] != "None" and key not in container_list:
                container_list[key] = {
                    "ns": lables["namespace"],
                    "pod": lables["pod"],
                    "con": lables["container"],
                    "serveutil": 0,
                }
            container_list[key][lables["value"]] = float(data[1])

        return [val for val in container_list.values()]

    def _query_serve_util_rate(self, ns: str, pod: str, con: str, end: float) -> float:
        task = (
            InstantQueryTask(
                "sysom_container_cfs_statis",
                end,
                aggregation="quantile_over_time",
                interval="7d",
                aggregation_val="0.01",
            )
            .append_equal_filter("exported_instance", self._instance)
            .append_equal_filter("namespace", ns)
            .append_equal_filter("pod", pod)
            .append_equal_filter("container", con)
            .append_equal_filter("bvt", "LS")
            .append_equal_filter("value", "serveutil")
        )
        result = self._metric_reader.instant_query([task])
        if result.code != 0:
            raise Exception(
                f"Query sysom_container_cfs_statis p99 serveutil data failed. code={result.code} err_msg={result.err_msg}"
            )
        if len(result.data) == 0:
            raise Exception(f"Query sysom_container_cfs_statis p99 serveutil empty.")
        return float(result.data[0].value[1])

    def diagnose(self) -> None:
        container_list = self._query_all_container()
        for container in container_list:
            cur_serveutil = container["serveutil"]
            # if no exec time skip it
            if cur_serveutil == 0:
                continue
            p99_serveutil = self._query_serve_util_rate(
                container["ns"], container["pod"], container["con"], self._ts
            )
            if cur_serveutil < p99_serveutil:
                qother = container["qother"]
                qslibing = container["qslibling"]
                self._container_table.append(
                    {
                        "key": str(uuid4()),
                        "容器": f"{container['ns']}-{container['pod']}-{container['con']}",
                        "CFS满足率(current/p99)": f"{cur_serveutil}/{p99_serveutil}",
                        "等待时间分布(other/slibing)": f"{qother}/{qslibing}",
                        "主要干扰原因": (
                            "Pod外部容器" if qother >= qslibing else "Pod内部容器"
                        ),
                    }
                )
                if qother < qslibing:
                    self._internal_err += 1
        if len(self._container_table) != 0:
            self._status = "ERROR"
            self._cause = (
                "Pod内部"
                if self._internal_err > len(self._container_table) - self._internal_err
                else "Pod外部"
            )
            self._suggestion = "对存在外部容器干扰的Pod调大CPU资源配额，对存在内部容器干扰的Pod调整容器数量"
        else:
            self._status = "NORMAL"

    def container_table(self) -> List[dict]:
        return self._container_table

    def overview(self) -> str:
        result = "系统无异常状态"

        if len(self._container_table) != 0:
            result = f"当前系统状态异常，有{len(self._container_table)}个受干扰容器，主要干扰来源于{self._cause}"
        return f"- **结论**：{result}。\n- **修复建议**：{self._suggestion}\n"

    def status(self) -> str:
        return self._status

    def suggestion(self) -> str:
        return self._suggestion

    def cause(self) -> str:
        return self._cause


class PreProcessor(DiagnosisPreProcessor):
    """Command diagnosis

    Just invoke command in target instance and get stdout result

    Args:
        DiagnosisPreProcessor (_type_): _description_
    """

    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        result = {"code": 0, "err_msg": "", "result": {}}
        try:
            instance = params.get("instance", "")
            # process host:port, we only use host
            instance = str(instance).split(":")[0]

            time_str = params.get("moment", "")
            dt = datetime.now() if time_str == "" else validate_time(time_str)
            diagnsoe = ServeUtilDiagnose(instance, dt)
            diagnsoe.diagnose()
            result["result"] = {
                "summary": {
                    "status": diagnsoe.status(),
                    "cause": diagnsoe.cause(),
                    "suggestion": diagnsoe.suggestion(),
                },
                "overview": {"data": diagnsoe.overview()},
                "container-table": {"data": diagnsoe.container_table()},
            }

        except Exception as e:
            logger.error(f"ServeUtil Diagnose failed. err={e}")
            traceback.print_exc()
            result = {
                "code": 1,
                "err_msg": f"{str(e)}\n 解决方法：请检查是否是混部场景、是否支持硬件指标采集、是否开启SysAK对应插件",
                "result": {
                    "summary": {
                        "status": "normal",
                        "cause": "N/A",
                        "suggestion": "N/A",
                    },
                    "overview": {"data": ""},
                    "container-table": {"data": []},
                },
            }
        finally:
            return DiagnosisTask(
                jobs=[DiagnosisJob(instance="", cmd="")],
                offline_mode=True,
                offline_results=[json.dumps(result)],
            )

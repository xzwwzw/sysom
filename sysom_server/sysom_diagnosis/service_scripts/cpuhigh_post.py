"""
Time                2023/07/31 14:54
Author:             suiya.lhl
Email               liuhailong@linux.alibaba.com
File                cpuhigh_post.py
Description:
"""
from typing import List
from .base import DiagnosisJobResult, DiagnosisPostProcessor, PostProcessResult
import json

class PostProcessor(DiagnosisPostProcessor):
    def parse_diagnosis_result(self, results: List[DiagnosisJobResult]) -> PostProcessResult:
        postprocess_result = PostProcessResult(
            code=0,
            err_msg="",
            result={}
        )
        rawdt = json.loads(results[0].stdout)
        postprocess_result.result = rawdt["datasources"]
        return postprocess_result

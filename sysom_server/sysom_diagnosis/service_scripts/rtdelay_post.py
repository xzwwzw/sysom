"""
Time                2023/07/25
Author:             dongyunyao (zhiying)
File                rtdelay_post.py
Description:
"""
from typing import List
from .base import DiagnosisJobResult, DiagnosisPostProcessor, PostProcessResult
import json
import time

class PostProcessor(DiagnosisPostProcessor):
    def parse_diagnosis_result(self, results: List[DiagnosisJobResult]) -> PostProcessResult:
        postprocess_result = PostProcessResult(
            code=0,
            err_msg="",
            result={}
        )
        
        log = results[0].stdout
        datas = []
        try:
            results = json.loads(log)
            for line in results:
                if "read_ts" not in line:
                    continue
                read_ts = int(line["read_ts"])
                ts = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(read_ts))
                datas.append({
                    "请求时间": ts,
                    "RT时延": line["rt_latency"],
                    "oncpu": line["on"],
                    "运行队列时间": line["runqueue"],
                    "存储时间": line["io"],
                    "纯网络时延": line["net"],
                    "服务器处理时间": line["server"],
                    "futex": line["futex"],
                    "mutex": line["lock"],
                    "其他": line["other"]
                })

        except:
            pass
        postprocess_result.result = {
            "request_set": {"data": datas},
            "summary": {"status": "normal", "cause": "Just shows real-time lantency", 
                        "suggestion": "Optimize the application based on the latency distribution."}
        }
        return postprocess_result        


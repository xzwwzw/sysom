#!/usr/bin/sh
# Please follow the input of this script
# input : kernel_version : eg. 4.19.91-26.an7.x86_64
# output : branch generated from the logic given by user from the given kernel_version 
# User who just fulfill the branch generating logic in function "generate_branch" will be enough
kernel_version="$1"
branch=""

generate_branch() {
    # finish your branch generating logic here ...
    a=${kernel_version##*-}
    b=${a%%.*}
    branch="Version_release_${b}"
}

generate_branch
echo $branch
# Generated by Django 3.2.16 on 2023-05-04 09:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hotfix', '0002_auto_20230303_1601'),
    ]

    operations = [
        migrations.AddField(
            model_name='ostypemodel',
            name='git_rule',
            field=models.CharField(default='', max_length=255, verbose_name='该系列的源码git正则规则'),
        ),
        migrations.AddField(
            model_name='ostypemodel',
            name='source_debuginfo',
            field=models.CharField(default='', max_length=255, verbose_name='该系列的debuginfo仓库地址'),
        ),
        migrations.AddField(
            model_name='ostypemodel',
            name='source_devel',
            field=models.CharField(default='', max_length=255, verbose_name='该系列的devel仓库地址'),
        ),
        migrations.AddField(
            model_name='ostypemodel',
            name='sync_status',
            field=models.IntegerField(blank=True, null=True, verbose_name='同步状态'),
        ),
        migrations.AddField(
            model_name='ostypemodel',
            name='synced_at',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='同步时间'),
        ),
    ]

import sys
from clogger import logger
from django.apps import AppConfig
from django.db.models.signals import post_migrate
from cec_base.admin import dispatch_admin

class HotfixConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.hotfix'

    def ready(self):
        from django.conf import settings
        if ('runserver' in sys.argv or 'manage.py' not in sys.argv):
            from sysom_utils import CmgPlugin, SysomFramework
            try:
                # 初始化 channel_job sdk
                hotfix_cec_topic_name = "hotfix_job"
                post_migrate.connect(initialization_subscribe, sender=self)
                admin = dispatch_admin(settings.SYSOM_CEC_URL)
                if admin.create_topic(hotfix_cec_topic_name):
                    logger.info(">>INIT_HOTFIX_VIEW : create hotfix_job cec topic success")
            except Exception as e:
                logger.info(str(e))
                logger.info(">> INIT_HOTFIX_VIEW : create hotfix_job cec topic failed")
            SysomFramework.init(settings.YAML_CONFIG) \
                .load_plugin_cls(CmgPlugin) \
                .enable_channel_job() \
                .start()
            
            # create CECListerner object for cec message listening
            from lib.function import CECListener
            ceclistener = CECListener(settings.YAML_CONFIG.get_service_config(), settings.SYSOM_CEC_URL, settings.SYSOM_CEC_HOTFIX_SERVER_MSG_TOPIC)
            if ceclistener is None:
                logger.error("INIT CECListener Failed...")
            ceclistener.run()
        else:
            # nothing to do when execute database migrations
            pass
        logger.info(">>>>> hotfix module loading success")
        


def initialization_subscribe(sender, **kwargs):
    load_subscribe_models_data()


def load_subscribe_models_data():
    try:
        from .models import SubscribeModel

        if not SubscribeModel.objects.filter().first():
            sub = SubscribeModel.objects.create(
                title="admin",
                )
            sub.users.add(*[1,])
            sub.save()
    except Exception as e:
        pass
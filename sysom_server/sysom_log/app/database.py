# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                database.py
Description:
"""
from datetime import datetime
from sqlalchemy import *
from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy.orm import sessionmaker
from conf.settings import SQLALCHEMY_DATABASE_URL
from sysom_utils import FastApiResponseHelper


engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={})

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# async_engine = create_async_engine(ASYNC_SQLALCHEMY_DATABASE_URL, connect_args={})
# async_session: AsyncSession = sessionmaker(
#     class_=AsyncSession, autocommit=False, autoflush=False,
#     bind=async_engine, expire_on_commit=False
# )


# Base = declarative_base()
@as_declarative()
class Base:
    id: int = Column(Integer, primary_key=True)
    ts: int = Column(Integer, comment="时间戳")
    create_at: DateTime = Column(DateTime, default=datetime.now, comment="创建时间")
    update_at: DateTime = Column(
        DateTime, default=datetime.now, onupdate=datetime.now, comment="更新时间"
    )


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


FastApiResponseHelper.bind_base_class(Base)

import { create } from 'zustand'

export const useGlobalVariablesStore = create((set) => ({
    data: [{
        name: 'Example',
        value: 'Example Value'
    }],
    setGlobalVariablesStore: (data) => set({ data }),
}))
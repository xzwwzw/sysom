import { useDrop } from "react-dnd";
import { Button, Card, Empty, Form } from "antd";
import styles from "./index.less";
import React, { useCallback, useRef, useLayoutEffect } from "react";
import { FormattedMessage } from "umi";
import { ComponentContainer } from "@/pages/diagnose/generate/Pannel/components/componentContainer";
import { componentConfigs } from "../../component-config";
import { ProCard } from "@ant-design/pro-card";
import { usePanelGeneratorStore } from "@/pages/diagnose/generate/Pannel/store/panelGeneratorStore";


/**
 * Row component container
 * @param {config: Object, isDragging: boolean} props 
 * @returns 
 */
function RowContainer(props) {
    const { configs, isDragging } = props
    const configStore = usePanelGeneratorStore((state) => state.data)
    const [canDrop, setCanDrop] = React.useState(true)
    const [{ isOverCurrent }, drop] = useDrop(() => ({
        accept: Object.keys(componentConfigs).map((key) => componentConfigs[key]).filter((item, index) => item.container === 'panels').map((value, index) => { return value.type }),
        drop(_item, monitor) {
            const didDrop = monitor.didDrop()
            if (!didDrop) {
                return configs
            }
        },
        hover: () => {
            if (configs.children.length >= 3) {
                // return false
                setCanDrop(false)
            } else {
                setCanDrop(true)
            }
        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            isOverCurrent: monitor.isOver({ shallow: true }),
            // canDrop: monitor.canDrop(),
        }),
    }), [configStore, configs])
    const [display, setDisplay] = React.useState(undefined)
    const containerRef = React.useRef()
    const prev = useRef(null);
    useLayoutEffect(() => {
        if (containerRef.current) {
            let currentContainerRef = props.type === 'taskform' ? containerRef.current.children[0] : containerRef.current.children
            if (prev.current === null || prev.current === undefined) {
                // mounted保留前状态
                prev.current = Array.from(currentContainerRef).map((item, index) => {
                    const rect = item.getBoundingClientRect();
                    return { dom: item, pos: { left: rect.left, top: rect.top } };
                });
            } else {
                prev.current.map(({ dom, pos }) => {
                    // 获取新状态
                    const rect = dom.getBoundingClientRect();
                    // invent 还原到初始状态
                    dom.style = `transform: translate(${pos.left - rect.left}px, ${pos.top - rect.top
                        }px); grid-column: span ${props.type === 'taskform' ? 1 : 3}`;
                    // play: 执行动画
                    dom.animate(
                        [
                            {
                                transform: `translate(${pos.left - rect.left}px, ${pos.top - rect.top
                                    }px)`,
                            },
                            {
                                transform: "translate(0,0)",
                            },
                        ],
                        {
                            duration: 300,
                            easing: "cubic-bezier(0,0,0.32,1)",
                            fill: "forwards",
                        }
                    );
                });
                prev.current = Array.from(currentContainerRef).map((item, index) => {
                    const rect = item.getBoundingClientRect();
                    return { dom: item, pos: { left: rect.left, top: rect.top } };
                });
            }
        }
    }, [configStore]);
    const onHover = (index) => {
        setDisplay(index)
    }
    const onLeave = () => {
        setDisplay(undefined)
    }
    const savePrev = useCallback(new Promise((resolve, reject) => {
        if (containerRef.current !== null && containerRef.current !== undefined) {
            let currentContainerRef = props.type === 'taskform' ? containerRef.current.children[0] : containerRef.current.children
            prev.current = Array.from(currentContainerRef).map((item, index) => {
                const rect = item.getBoundingClientRect();
                return { dom: item, pos: { left: rect.left, top: rect.top } };
            });
        }
        resolve(prev)
    }), [configStore])
    const renderComponent = useCallback((config, index) => {
        if (config !== undefined) {
            return (
                <div className={styles.item} onMouseEnter={() => onHover(index)} onMouseLeave={onLeave} key={index}>
                    <ComponentContainer value={config} displayIndex={display} index={index} savePrev={savePrev} rowKey={configs.key} />
                </div>
            )
        }
    }, [configStore, display])

    const dropTypeSwitch = useCallback((type) => {
        if (configs.children.length !== 0) {
            return (
                <div ref={containerRef} className={styles.itemFlex}>
                    {
                        configs.children?.map((value, index) => renderComponent(value, index))
                    }
                </div>
            )
        } else {
            return (
                <>
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}
                        description={<span><FormattedMessage id="pages.diagnose.generatePanel.noComponent" defaultMessage="暂时没有组件，从左边拖入试试吧" /></span>} />
                </>
            )
        }
    }, [configStore, display])

    return (
        <ProCard collapsible title={configs.title} style={{ zIndex: 99, position: 'relative', minHeight: '0px', ...configs.style, overflowY: 'auto' }} size="small" ref={drop} className={styles.sectionContainer}>
            <div style={{ zIndex: 1 }} className={`${styles.dropIndicator} ${isDragging ? styles.dragging : ''} ${isOverCurrent ? !canDrop ? styles.cannot : styles.hovering : ''}`}>
            </div>
            {
                dropTypeSwitch(props.type)
            }
        </ProCard>
    );
}
export default RowContainer;
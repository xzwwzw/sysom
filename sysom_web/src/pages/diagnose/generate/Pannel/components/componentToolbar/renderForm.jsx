import {
    ProForm,
    ProFormList,
    ProFormText,
    ProFormSelect,
} from "@ant-design/pro-components";
import { ProCard } from '@ant-design/pro-card'
import styles from './index.less'
import { ProFormDigit, ProFormGroup } from '@ant-design/pro-form';
import { useIntl } from 'umi'
import { componentConfigs } from "../../component-config";

/**
 * Generate a form from a json
 * @param {Object} item component config
 * @param {Object} value component config value
 * @param {string[]} name component name list
 * @param {string[]} lastType last component type
 * @returns 
 */
const DynamicForm = ({ item, value, name, lastType }) => {
    const intl = useIntl()
    switch (item.type) {
        case 'text':
            return (
                <ProFormText
                    width={item.width}
                    name={name ? lastType === 'list' || lastType === 'key-list' ? name.slice(0, -1).concat([item.name]) : name.concat([item.name]) : [item.name]}
                    label={item.label + ` (${item.name})`}
                    placeholder={item.placeholder}
                    initialValue={lastType === 'key-list' ? value : value[item.name] ?? ''}
                />
            )
        case 'list':
            if (!item.add&&item.add!==undefined) {
                return (
                    <></>
                )
            }
            return (
                <ProForm.Group title={item.label + ` (${item.name})`}>
                    <ProFormList
                        name={name ? lastType === 'list' || lastType === 'key-list' ? name.slice(0, -1).concat([item.name]) : name.concat([item.name]) : [item.name]}
                        initialValue={value[item.name] ?? ''}
                        creatorButtonProps={{
                            position: 'bottom',
                            creatorButtonText: intl.formatMessage({ id: 'pages.diagnose.generatePanel.newLine', defaultMessage: '再建一行' }),
                        }}
                        itemRender={({ listDom, action }, { index }) => {
                            return (
                                <ProCard
                                    bordered
                                    extra={action}
                                    direction='column'
                                    wrap
                                    size='default'
                                    title={`${item.label} ${index + 1}`}
                                    className={styles.listCard}
                                    style={{
                                        marginBlockEnd: 8,
                                        color: 'white'
                                    }}
                                >
                                    {listDom}
                                </ProCard>
                            );
                        }
                        }
                    >
                        <ProForm.Group>
                            {
                                item.form.map((innerItem, index) => {
                                    return <DynamicForm item={innerItem} value={value[item.name] ?? ''} name={[item.name]} key={index} lastType={'list'} />
                                })
                            }
                        </ProForm.Group>
                    </ProFormList>
                </ProForm.Group>
            )
        case 'key-list':
            // to handle some json data edit like
            // "options": {
            //     "正常": { "color": "blue" },
            //     "危险": { "color": "red", },
            //     "success": { "color": "green", "text": "成功" }
            // }
            // not in array format
            // convert json to array first
            const jsonToArray = (json) => {
                let arr = []
                for (let key in json) {
                    arr.push({
                        key: key,
                        ...json[key]
                    })
                }
                return arr
            }
            const arrayToJson = (arr) => {
                let json = {}
                arr.forEach((item) => {
                    json[item.key] = item
                })
                return json
            }
            const keyList = jsonToArray(value)
            return (
                <ProForm.Group title={item.label + ` (${item.name})`}>
                    <ProFormList
                        name={name ? lastType === 'list' || lastType === 'key-list' ? name.slice(0, -1).concat([item.name]) : name.concat([item.name]) : [item.name]}
                        creatorButtonProps={{
                            position: 'bottom',
                            creatorButtonText: intl.formatMessage({ id: 'pages.diagnose.generatePanel.newLine', defaultMessage: '再建一行' }),
                        }}
                        initialValue={keyList}
                        max={item.max}
                        itemRender={({ listDom, action }, { index }) => {
                            return (
                                <ProCard
                                    bordered
                                    extra={action}
                                    direction='column'
                                    wrap
                                    size='default'
                                    title={`${item.label} ${index + 1}`}
                                    className={styles.listCard}
                                    style={{
                                        marginBlockEnd: 8,
                                        color: 'white'
                                    }}
                                ><ProFormGroup direction='column'>
                                        {listDom}
                                    </ProFormGroup>
                                </ProCard>
                            );
                        }
                        }
                    >

                        <ProFormText
                            width={item.key.width}
                            name={name.concat(['key'])}
                            label={item.key.label}
                            placeholder={item.key.placeholder}
                        />
                        {/* <ProFormGroup title={`${item.label}参数配置`}>
                            {
                                item.value.map((innerItem, index) => {
                                    return <DynamicForm item={innerItem} value={keyList ?? ''} name={name.concat([innerItem.name])} key={index} lastType={'key-list'} />
                                })
                            }
                        </ProFormGroup> */}
                    </ProFormList>
                </ProForm.Group >
            )
        case 'field':
            return (
                <ProForm.Group title={item.label + ` (${item.name})`} style={{ flexDirection: 'columns' }}>
                    {
                        item.children.map((innerItem, index) => {
                            return <DynamicForm item={innerItem} value={lastType === 'key-list' ? value : value[item.name] ?? ''} name={name ? lastType === 'list' || lastType === 'key-list' ? name.slice(0, -1).concat([item.name]) : name.concat([item.name]) : [item.name]} key={index} />
                        })
                    }
                </ProForm.Group>
            )
        case 'select':
            let options = []
            if (item.options === 'panel-types') {
                options = Object.keys(componentConfigs).map((key) => componentConfigs[key]).filter((item, index) => item.container === 'panels').map((value, index) => { return value.name })
            }
            else if (item.options === 'taskform-types') {
                options = Object.keys(componentConfigs).map((key) => componentConfigs[key]).filter((item, index) => item.container === 'taskform').map((value, index) => { return value.name })
            } else if (item.options === 'all-types') {
                options = Object.keys(componentConfigs).map((key) => componentConfigs[key]).map((value, index) => { return value.name })
            }
            return (
                <ProFormSelect
                    width={item.width}
                    name={name ? lastType === 'list' || lastType === 'key-list' ? name.slice(0, -1).concat([item.name]) : name.concat([item.name]) : [item.name]}
                    label={item.label + ` (${item.name})`}
                    placeholder={item.placeholder}
                    initialValue={lastType === 'key-list' ? value : value[item.name] ?? ''}
                    options={options.length ? options : item.options}
                />
            )
        case 'digit':
            return (
                <ProFormDigit
                    width={item.width}
                    name={name ? lastType === 'list' || lastType === 'key-list' ? name.slice(0, -1).concat([item.name]) : name.concat([item.name]) : [item.name]}
                    label={item.label + ` (${item.name})`}
                    placeholder={item.placeholder}
                    initialValue={lastType === 'key-list' ? value : value[item.name] ?? ''}
                    addonAfter={item.addonAfter}
                />
            )
    }
};

export default DynamicForm;

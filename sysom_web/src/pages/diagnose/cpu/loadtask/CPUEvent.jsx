import { Statistic } from 'antd';
import { useState } from 'react';
import ProCard from '@ant-design/pro-card';
import RcResizeObserver from 'rc-resize-observer';
import { useIntl, FormattedMessage } from 'umi';
import styles from '../../diagnose.less';

const { Divider } = ProCard;

const DiagExtra = (props) => {
  return (
    <>
      <div className={styles.titname}><FormattedMessage id="pages.diagnose.diagnosisid" defaultMessage="Diagnosis ID:" /></div>
      <div className={styles.titneir}>{props.dataSour.task_id}</div>
      <div className={styles.titname}><FormattedMessage id="pages.diagnose.diagnosistime" defaultMessage="Diagnosis time:" /></div>
      <div className={styles.titneir}>{props.dataSour.created_at}</div>
    </>
  )
}

export default (props) => {
  const intl = useIntl();
  const [responsive, setResponsive] = useState(false);
  return (
    <RcResizeObserver
      key="resize-observer"
      onResize={(offset) => {
        setResponsive(offset.width < 596);
      }}
    >
      <ProCard
        title={props.title}
        extra={[
          <DiagExtra dataSour={props.data} key="diagextra" />,
        ]}
        split={responsive ? 'horizontal' : 'vertical'}
        headerBordered
      >
        <ProCard.Group title={props.subtitle} direction={responsive ? 'column' : 'row'}>
          <ProCard>
            <Statistic title={intl.formatMessage({
              id: 'pages.diagnose.averagesystemload',
              defaultMessage: 'Average system load',
            })} value={props.data.result.result.loadavg}
              valueStyle={{ color: (props.data.result.result.loadavg) > "50" ? "red" : "green" }} />
          </ProCard>
          <Divider type={responsive ? 'horizontal' : 'vertical'} />
          <ProCard>
            <Statistic title={intl.formatMessage({
              id: 'pages.diagnose.sysinfluencedetection',
              defaultMessage: 'Sys influence detection',
            })} value={(props.data.result.result.sys) === "false" ? <FormattedMessage id="pages.diagnose.normal" defaultMessage="Normal" /> : <FormattedMessage id="pages.diagnose.danger" defaultMessage="Danger" />}
              valueStyle={{ color: (props.data.result.result.sys) === "false" ? "green" : "red" }} />
          </ProCard>
          <Divider type={responsive ? 'horizontal' : 'vertical'} />
          <ProCard>
            <Statistic title={intl.formatMessage({
              id: 'pages.diagnose.hardwaredetection',
              defaultMessage: 'Hardware interrupt affects detection',
            })} value={(props.data.result.result.irq) === "false" ? <FormattedMessage id="pages.diagnose.normal" defaultMessage="Normal" /> : <FormattedMessage id="pages.diagnose.danger" defaultMessage="Danger" />}
              valueStyle={{ color: (props.data.result.result.irq) === "false" ? "green" : "red" }} />
          </ProCard>
          <Divider type={responsive ? 'horizontal' : 'vertical'} />
          <ProCard>
            <Statistic title={intl.formatMessage({
              id: 'pages.diagnose.Softdetection',
              defaultMessage: 'Soft interrupt affects detection',
            })} value={(props.data.result.result.softirq) === "false" ? <FormattedMessage id="pages.diagnose.normal" defaultMessage="Normal" /> : <FormattedMessage id="pages.diagnose.danger" defaultMessage="Danger" />}
              valueStyle={{ color: (props.data.result.result.softirq) === "false" ? "green" : "red" }} />
          </ProCard>
          <Divider type={responsive ? 'horizontal' : 'vertical'} />
          <ProCard>
            <Statistic title={intl.formatMessage({
              id: 'pages.diagnose.IOdetection',
              defaultMessage: 'IO influence detection',
            })} value={(props.data.result.result.io) === "false" ? <FormattedMessage id="pages.diagnose.normal" defaultMessage="Normal" /> : <FormattedMessage id="pages.diagnose.danger" defaultMessage="Danger" />}
              valueStyle={{ color: (props.data.result.result.io) === "false" ? "green" : "red" }} />
          </ProCard>
        </ProCard.Group>
      </ProCard>
    </RcResizeObserver>
  );
}
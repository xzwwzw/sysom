/*
 * @Author: wb-msm241621
 * @Date: 2023-12-25 11:04:40
 * @LastEditTime: 2024-01-04 17:57:38
 * @Description: 
 */

import { ModalForm, ProFormText, ProFormTextArea, ProFormUploadDragger, ProFormSelect } from '@ant-design/pro-form'
import { Button } from 'antd'
import { useIntl, FormattedMessage } from 'umi';
import { sbsOfflineTaskResult } from '../service'


const UploadResultFormModal = (props) => {
    const intl = useIntl()

    const uploadResultEvent = async (params) => {
        let status = true
        if (params.content_encoding == "base64") {
            params['results'] = btoa(params.results)
        }
        const formData = new FormData()
        for (let key in params) {
            if ('files' == key) {
                const files = params[key].map((item) => { return item.originFileObj })
                formData.append(key, files)
            } else {
                formData.append(key, params[key])
            }
        }

        await sbsOfflineTaskResult(formData).then((res) => {
            props.onSuccessCallBack()
        }).catch((error) => {
            status = false
        })
        return status
    }

    return <ModalForm
        title={
            intl.formatMessage({
                id: 'pages.diagnose.uploadtaskresultfrom',
                defaultMessage: 'upload task result from',
            })
        }
        initialValues={props.record}
        width="440px"
        trigger={<Button type='link'>
            <FormattedMessage id="pages.diagnose.uploadtaskresult" defaultMessage="upload task result" />
        </Button>}
        onFinish={async (params) => {
            return await uploadResultEvent(params)
        }}
    >

        <ProFormText
            width="md"
            name="task_id"
            disabled
            label={intl.formatMessage({
                id: "pages.diagnose.diagnosisID",
                defaultMessage: "Diagnosis ID"
            })}
        />
        <ProFormTextArea
            width="md"
            name="results"
            rules={[
                { required: true, message: 'field offline task result required!' }
            ]}
            label={intl.formatMessage({ id: "pages.diagnose.offlinetaskresult", defaultMessage: "offline task result" })}
        />
        <ProFormSelect
            width="md"
            valueEnum={{
                text: 'text',
                base64: 'base64',
            }}
            initialValue={"text"}
            name="content_encoding"
            label={intl.formatMessage({ id: "pages.diagnose.selectencoding", defaultMessage: "select encoding result" })}
        />
        <ProFormUploadDragger
            name="files"
            label={
                intl.formatMessage({
                    id: "pages.diagnose.offlinetaskresultannex",
                    defaultMessage: "offline task result annex"
                })}
            fieldProps={{
                beforeUpload: () => { return false }
            }}
        />

    </ModalForm>
}

export default UploadResultFormModal;

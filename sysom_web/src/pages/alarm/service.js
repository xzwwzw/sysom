// @ts-ignore

/* eslint-disable */
import {
  request
} from 'umi';

export async function getAlarmDataList(params, options) {
  const token = localStorage.getItem("token")
  const msg = await request("/api/v1/alarm/list", {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: params,
   ...(options || {}),
  })
  return msg
}

export async function markAlarmDataAsReadByAlertId(alert_ids, options) {
  const token = localStorage.getItem("token")
  const msg = await request("/api/v1/alarm/mark_as_read", {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      alert_ids: alert_ids
    },
   ...(options || {}),
  })
  return msg
}

export async function markAlarmDataAsRead(ids, options) {
  const token = localStorage.getItem("token")
  const msg = await request("/api/v1/alarm/mark_as_read", {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      ids: ids
    },
   ...(options || {}),
  })
  return msg
}
import React, { useRef } from "react";
import { PageContainer } from "@ant-design/pro-layout";
import ProTable from "@ant-design/pro-table";
import { getVmcore } from "../service";
import TextArea from "antd/lib/input/TextArea";
import { useIntl, FormattedMessage } from 'umi';

const VmcoreMatch = () => {
  const actionRef = useRef();
  const intl = useIntl();
  const columns = [
    {
      title: <FormattedMessage id="pages.vmcore.hostname" defaultMessage="Host name" />,
      dataIndex: "hostname",
      valueType: "textarea",
      hideInSearch: true,
    },
    {
      title: <FormattedMessage id="pages.vmcore.similarstack" defaultMessage="Similar stack" />,
      dataIndex: "similar_dmesg",
      valueType: "textarea",
      colSize: 4,
      hideInTable: true,
      renderFormItem: (_, { type, defaultRender, formItemProps, fieldProps, ...rest }, form) => {
        const status = form.getFieldValue("state");
        if (status !== "open") {
          return <TextArea {...fieldProps} rows={8} />;
        }
        return defaultRender(_);
      },
    },
    {
      title: "IP",
      dataIndex: "ip",
      hideInSearch: true,
      valueType: "textarea",
    },
    {
      title: <FormattedMessage id="pages.vmcore.downtime" defaultMessage="Down time" />,
      dataIndex: "core_time",
      sorter: true,
      hideInSearch: true,
      valueType: "dateTime",
    },
    {
      title: <FormattedMessage id="pages.vmcore.kernelversion" defaultMessage="Kernel version" />,
      dataIndex: "ver",
      valueType: "texarea",
      hideInSearch: true,
    },
    {
      title: "Vmcore",
      dataIndex: "vmcore_check",
      hideInSearch: true,
      valueEnum: {
        false: {
          text: <FormattedMessage id="pages.vmcore.no" defaultMessage="No" />,
          status: "Default",
        },
        true: {
          text: <FormattedMessage id="pages.vmcore.yes" defaultMessage="Yes" />,
          status: "Success",
        },
      },
    },
    {
      title: <FormattedMessage id="pages.vmcore.solution" defaultMessage="Solution" />,
      dataIndex: "issue_check",
      hideInSearch: true,
      valueEnum: {
        false: {
          text: <FormattedMessage id="pages.vmcore.no" defaultMessage="No" />,
          status: "Default",
        },
        true: {
          text: <FormattedMessage id="pages.vmcore.yes" defaultMessage="Yes" />,
          status: "Success",
        },
      },
    },
    {
      title: <FormattedMessage id="pages.vmcore.similarity" defaultMessage="Similarity" />,
      dataIndex: "rate",
      hideInSearch: true,
      valueType: "textarea",
    },
    {
      title: <FormattedMessage id="pages.vmcore.outagedetails" defaultMessage="Outage details" />,
      dataIndex: "option",
      valueType: "option",
      render: (_, record) => [
        <a key="showDetail" href={"/vmcore/detail/" + record.id}>
          <FormattedMessage id="pages.vmcore.view" defaultMessage="View" />
        </a>,
      ],
    },
  ];
  return (
    <PageContainer>
      <ProTable
        headerTitle={intl.formatMessage({
          id: 'pages.vmcore.list',
          defaultMessage: 'Outage list',
        })}
        actionRef={actionRef}
        rowKey="id"
        request={getVmcore}
        columns={columns}
        pagination={{ pageSize: 5 }}
      />
    </PageContainer>
  );
};
export default VmcoreMatch;

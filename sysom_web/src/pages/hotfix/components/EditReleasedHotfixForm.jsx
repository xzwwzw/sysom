/*
 * @Author: wb-msm241621
 * @Date: 2023-12-06 18:07:26
 * @LastEditTime: 2024-01-03 11:04:50
 * @Description: 
 */
import { useState, useRef } from 'react';
import { Button } from "antd";
import { EditOutlined } from "@ant-design/icons";
import { DrawerForm, ProFormText, ProFormSelect, ProFormSwitch, ProFormDateTimePicker, ProFormTextArea } from "@ant-design/pro-form";
import { useIntl, FormattedMessage } from 'umi';
import { updateChangeReleasedHotfixInfo } from '../service'


const EditReleasedHotfixForm = (props) => {
    const intl = useIntl();
    const editReleasedHotfixFormRef = useRef()
    const [readonlyone, setReadonlyOne] = useState(!props.record.deprecated);
    const KernelConfigChange = (e) => { setReadonlyOne(!e); }
    return <DrawerForm
        formRef={editReleasedHotfixFormRef}
        title={
            intl.formatMessage(
                { id: 'pages.hotfix.edit_released_hotfix', defaultMessage: "edit released hotfix" }
            )
        }
        width="440px"
        trigger={
            <Button type="link" icon={<EditOutlined />}>
                {intl.formatMessage({ id: 'pages.hotfix.edit', defaultMessage: "Edit" })}
            </Button>
        }
        onFinish={async (value) => {
            value['id'] = props.record.id
            const result = await updateChangeReleasedHotfixInfo(props.record.id, value)
            if (result.success) { props.editHotfixReleasedSuccess() } else { }
            return true
        }}
        initialValues={props.record}
    >
        <ProFormText
            disabled
            width="md"
            name="hotfix_id"
            label={intl.formatMessage({ id: 'pages.hotfix.hotfixid', defaultMessage: 'Hotfix ID' })}
            tooltip={intl.formatMessage({
                id: 'pages.hotfix.tooltips.hotfixid',
                defaultMessage: 'The Hotfix ID'
            })}
        />
        <ProFormText
            disabled
            name="released_kernel_version"
            width="md"
            tooltip={intl.formatMessage({
                id: 'pages.hotfix.tooltips.released_kernelverison',
                defaultMessage: 'pages.hotfix.tooltips.released_kernelverison'
            })}
            label={intl.formatMessage({
                id: 'pages.hotfix.released_kernel_version',
                defaultMessage: 'Released kernel version'
            })}
        />
        <ProFormSelect
            name="serious"
            valueEnum={{
                0: "可选安装",
                1: "建议安装",
                2: "需要安装"
            }}
            options={[
                { label: '可选安装', value: 0 },
                { label: '建议安装', value: 3 },
                { label: '需要安装', value: 2 },
            ]}
            width="md"
            tooltip={intl.formatMessage({
                id: 'pages.hotfix.tooltips.serious_level',
                defaultMessage: 'The suggestion level of installing this hotfix'
            })}
            label={intl.formatMessage({
                id: 'pages.hotfix.serious',
                defaultMessage: 'Serious Level'
            })}
        />
        <ProFormTextArea
            width="md"
            name="serious_explain"
            label={intl.formatMessage({
                id: 'pages.hotfix.serious_explain',
                defaultMessage: 'Serious Explain'
            })}
        />
        <ProFormTextArea
            name="description"
            width="md"
            tooltip={intl.formatMessage({
                id: 'pages.hotfix.tooltips.description',
                defaultMessage: 'The description of this hotfix.'
            })}
            label={intl.formatMessage({
                id: 'pages.hotfix.description',
                defaultMessage: 'Description'
            })}
        />
        <ProFormSelect
            name="fix_system"
            width="md"
            options={[
                {
                    label: "调度",
                    value: 0,
                },
                {
                    label: "内存",
                    value: 1,
                },
                {
                    label: "网络",
                    value: 2,
                },
                {
                    label: "存储",
                    value: 3,
                },
                {
                    label: "其他",
                    value: 4,
                }
            ]}
            tooltip={intl.formatMessage({
                id: 'pages.hotfix.tooltips.fix_system',
                defaultMessage: 'The Sub system this hotfix fixed',
            })}
            label={intl.formatMessage({
                id: 'pages.hotfix.fix_system',
                defaultMessage: 'Fixed Subsystem'
            })}
        />
        <ProFormText
            name="download_link"
            width="md"
            tooltip={intl.formatMessage({
                id: 'pages.hotfix.tooltips.downloadlink',
                defaultMessage: 'Please input the download link of this released hotfix'
            })}
            label={intl.formatMessage({
                id: 'pages.hotfix.download_link',
                defaultMessage: 'Download Link'
            })}
            rules={[
                {
                    pattern: /^https?:\/\/\S+/,
                    message: (
                        <FormattedMessage
                            id="pages.hotfix.download_link_error"
                            defaultMessage="download_link_error"
                        />
                    ),
                }
            ]}
        />
        <ProFormDateTimePicker
            name="released_time"
            width="md"
            tooltip={intl.formatMessage({
                id: 'pages.hotfix.tooltips.released_time',
                defaultMessage: 'Please input the released time of this released hotfix'
            })}
            label={intl.formatMessage({
                id: 'pages.hotfix.released_time',
                defaultMessage: 'Released Time'
            })}
        />
        <ProFormSwitch
            style={{
                marginBlockEnd: 16,
            }}
            width="md"
            initialValue={props.record.deprecated}
            name="deprecated"
            label={intl.formatMessage({
                id: 'pages.hotfix.deprecated',
                defaultMessage: 'Deprecated'
            })}
            checkedChildren={intl.formatMessage({
                id: 'pages.hotfix.yes',
                defaultMessage: 'Yes'
            })}
            unCheckedChildren={intl.formatMessage({
                id: 'pages.hotfix.no',
                defaultMessage: 'No'
            })}
            onChange={KernelConfigChange}
        />
        <ProFormText
            name="deprecated_info"
            width="md"
            hidden={false}
            tooltip={intl.formatMessage({
                id: 'pages.hotfix.tooltips.deprecated_info',
                defaultMessage: 'Please input the deprecated infomation of this released hotfix'
            })}
            label={intl.formatMessage({
                id: 'pages.hotfix.deprecated_info',
                defaultMessage: 'Deprecated Info'
            })}
            disabled={readonlyone}
        />
    </DrawerForm>
}

export default EditReleasedHotfixForm;

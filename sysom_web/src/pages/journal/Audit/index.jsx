import {  useRef } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { getAudit, getResponseCode } from '../service';


const request = async (params) => {
  const response = await getResponseCode(params)
  return response.data;
};


const AuditList = () => {
  const actionRef = useRef();
  const intl = useIntl();

  const columns = [
    {
      title: <FormattedMessage id="pages.journal.audit.create_at" defaultMessage="create_at" />,
      dataIndex: 'create_at',
      valueType: 'dateTime',
      hideInSearch: true
    },
    {
      title: <FormattedMessage id="pages.journal.audit.username" defaultMessage="username" />,
      dataIndex: 'username',
      hideInSearch: true
    },
    {
      title: <FormattedMessage id="pages.journal.audit.ip" defaultMessage="ip" />,
      dataIndex: 'ip',
      valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.journal.audit.path" defaultMessage="path" />,
      dataIndex: 'path',
      valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.journal.audit.methond" defaultMessage="methond" />,
      dataIndex: 'methond',
      valueType: 'textarea',
      valueEnum: {
        'GET': { text: 'GET' },
        'POST': { text: 'POST' },
        'PUT': { text: 'PUT' },
        'DELETE': { text: 'DELETE' },
        'PATCH': { text: 'PATCH' },
      }
    },
    {
      title: <FormattedMessage id="pages.journal.audit.status" defaultMessage="status" />,
      dataIndex: 'status',
      valueType: 'select',
      request,
      params: {}
    },
    {
      title: <FormattedMessage id="pages.journal.audit.request_type" defaultMessage="request_type" />,
      dataIndex: 'request_type',
      valueEnum: {
        'login': {
          text: (
            <FormattedMessage id="pages.journal.audit.login" defaultMessage="login" />
          ),
        },
        'operate': {
          text: (
            <FormattedMessage id="pages.journal.audit.operate" defaultMessage="operate" />
          ),
        },
        'logout': {
          text: (
            <FormattedMessage id="pages.journal.audit.logout" defaultMessage="logout" />
          )
        }
      },
    }
  ];
  return (
    <PageContainer>
      <ProTable
        headerTitle={intl.formatMessage({
          id: 'pages.journal.audit.title',
          defaultMessage: 'Audit',
        })}
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
        ]}
        pagination={{ showSizeChanger: true, pageSizeOptions: [10, 20], defaultPageSize: 10 }}
        request={getAudit}
        columns={columns}
      />
    </PageContainer>
  );
};

export default AuditList;

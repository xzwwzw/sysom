/// <reference types="cypress" />


describe("SysOM Alarm Manager Test", () => {
    beforeEach(() => {
        cy.login()
    })
    it.only("alarm list", () => {
        cy.intercept("GET", "/api/v1/alarm/list?*").as("getAlarmList")

        cy.visit("alarm/list")

        cy.wait("@getAlarmList").then((interception) => {
            expect(interception).to.have.property('response')
            expect(interception.response?.body.code, 'code').to.equal(200)
            expect(interception.response.statusCode).to.equal(200)

            // cy.get('.ant-table-tbody').find('tr').should("have.length.gte", 0)
            cy.wait(1000)
            cy.get('.ant-table-content').find('table').then(($el) => {
                if ($el.text().includes("No data")) {
                    cy.wrap($el).contains("No data")
                } else {
                    // 断言告警列表数据内容大于等于1
                    cy.wrap($el).find('.ant-table-tbody').find('tr').should('have.length.gte', 1)

                    // 断言告警表格有11个字段
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').should('have.length.gte', 9)

                    const td = cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td')
                    // 断言告警ID格式是否为UUID V4
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').eq(2).find('span').invoke('text').should('match', /^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-f]{12}$/i)

                    // 断言告警级别是否在枚举类型中
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').eq(5).invoke("text").then((text) => {
                        expect(text.trim()).to.be.oneOf(['监控告警', '应用告警', '其他告警'])
                    })

                    // 断言告警级别是否在枚举类型中
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').eq(6).invoke("text").then((text) => {
                        expect(text.trim()).to.be.oneOf(['严重', '警告', '错误'])
                    })

                    // 断言告警处理状态是否在枚举类型中
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').eq(8).invoke("text").then((text) => {
                        expect(text.trim()).to.be.oneOf(['已读', '未读'])
                    })

                    // 断言告警时间格式是否正确
                    cy.wrap($el).find('.ant-table-tbody').find('tr').eq(0).find('td').eq(9).invoke("text").should('match', /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/)
                }
            })
        })

        cy.wait(1000)
        cy.get('.ant-pro-form-collapse-button').contains("展开").click()

        cy.get('#alert_level').click()
        cy.get(".rc-virtual-list-holder-inner").contains("警告").click()

        cy.get('#alert_category').click()
        cy.get(".rc-virtual-list-holder-inner").contains("应用告警").click()
        
        cy.get('#deal_status').click()
        cy.get(".rc-virtual-list-holder-inner").contains("未读").click()

        // 点击查询
        cy.get(':nth-child(2) > .ant-btn').click()

        cy.wait(1000)

        cy.get('.ant-table-tbody').then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("tr").should("have.length.gte", 1)
                cy.wrap($el).find("tr").eq(0).contains("标记已读").click()
            }
        })
        
        // 点击重置
        cy.get('.ant-space > :nth-child(1) > .ant-btn').click()
        // 点击收起
        cy.get('.ant-pro-form-collapse-button').contains("收起").click()
    })
})
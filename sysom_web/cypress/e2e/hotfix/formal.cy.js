/* ==== Test Created with Cypress Studio ==== */

/**
 * This is a one singal formal hotfix list test.
 * This test dont need any successfully build.
 */

it('formal hotfix test', function() {
  cy.login()
  /* ==== Generated with Cypress Studio ==== */
  cy.visit('/hotfix/formal_hotfix');
  cy.get('#created_at').click();
  cy.get('.ant-picker-today-btn').click();
  cy.get(':nth-child(1) > .ant-btn > span').click({multiple: true});
  cy.get('#hotfix_name').clear('te');
  cy.get('#hotfix_name').type('test');
  cy.get('#patch_file').type('patch_file_0001.patch');
  cy.get('#rc_select_1').type('5.10.112-11.1.al8.x86_64');
  cy.get(':nth-child(2) > .ant-btn > span').click();
  
  /* ==== End Cypress Studio ==== */
});

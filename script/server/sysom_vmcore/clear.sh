#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-vmcore

clear_app() {
    sed -i '/vmcore/d' /var/spool/cron/root
    sed -i '/vmcore/d' /etc/exports
    rm -rf ${SERVER_HOME}/vmcore
    exportfs -rv
    systemctl stop nfs-server
    systemctl stop rpcbind
    systemctl stop nfs
    rm -rf /etc/supervisord.d/${SERVICE_NAME}.ini
    ###use supervisorctl update to stop and clear services###
    supervisorctl update
}

# Stop first
bash -x $BaseDir/stop.sh

clear_app

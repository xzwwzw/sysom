#!/bin/bash
SERVICE_NAME=sysom-rca

is_start() {
    supervisorctl status ${SERVICE_NAME}
    if [ $? -eq 0 ]; then
        return 1
    else
        return 0
    fi
}

start_app() {
    is_start
    if [[ $? == 0 ]]; then
        supervisorctl start $SERVICE_NAME
        is_start
        if [[ $? == 0 ]]; then
            echo "${SERVICE_NAME} service start fail, please check log"
            exit 1
        else
            echo "supervisorctl start ${SERVICE_NAME} success..."
        fi
    fi
}

start_app

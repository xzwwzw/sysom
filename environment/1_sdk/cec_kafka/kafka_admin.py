# -*- coding: utf-8 -*- #
"""
Time                2023/12/11 20:20
Author:             mingfeng (SunnyQjm), zhangque (Wardenjohn)
Email               mfeng@linux.alibaba.com, ydzhang@linux.alibaba.com
File                kafka_admin.py
Description:        Kafka Admin. For Kafka manage object.
"""
import json
import uuid

from cec_base.admin import Admin, ConsumeStatusItem
from cec_base.exceptions import TopicNotExistsException, TopicAlreadyExistsException
from cec_base.exceptions import ConsumerGroupAlreadyExistsException, CecException
from cec_base.event import Event
from cec_base.url import CecUrl
from cec_base.meta import TopicMeta, PartitionMeta, ConsumerGroupMeta, \
    ConsumerGroupMemberMeta
from clogger import logger
from confluent_kafka.admin import AdminClient, NewTopic
from confluent_kafka import Consumer as ConfluentKafkaConsumer
from confluent_kafka import Consumer, TopicPartition
import confluent_kafka
from confluent_kafka.error import KafkaException, KafkaError
from .utils import raise_if_not_ignore
from .common import StaticConst, ClientBase
class KafkaAdmin(Admin):
    """This is a kafka-based execution module implement of Admin

    KafkaAdmin contain the function of managing topic and group, etc.

    https://github.com/confluentinc/confluent-kafka-python/blob/master/examples/list_offsets.py
    """

    _EVENT_KEY_KAFKA_CONSUMER_MESSAGE = "_EVENT_KEY_KAFKA_CONSUMER_MESSAGE"

    """__init__ for object KafkaAdmin
    Args:
        url: CecUrl Object. CecUrl contains cec url format definition
    """
    def __init__(self, url: CecUrl) -> None:
        super().__init__()
        self._kafka_admin_client: AdminClient = None
        self._kafka_consumer_client: ConfluentKafkaConsumer = None
        self._current_url: str = ""
        self._current_cec_url: CecUrl= url
        url.params = StaticConst.parse_kafka_connection_params(url.params)
        self.connect_by_cec_url(url)

    @staticmethod
    def static_create_topic(kafka_admin_client: AdminClient,
                            topic_name: str = "", num_partitions: int = 1,
                            replication_factor: int = 1,
                            ignore_exception: bool = False,
                            expire_time: int = 24 * 60 * 60 * 1000) -> bool:
        try:
            res = kafka_admin_client.create_topics(
                [NewTopic(topic_name, num_partitions, replication_factor)])
            res.get(topic_name).result()
        except KafkaException as ke:
            if ke.args[0].code() == KafkaError.TOPIC_ALREADY_EXISTS:
                return raise_if_not_ignore(ignore_exception,
                                           TopicAlreadyExistsException(
                                               f"Topic {topic_name} already "
                                               f"exists."
                                           ))
            else:
                return raise_if_not_ignore(ignore_exception, ke)
        except Exception as e:
            return raise_if_not_ignore(ignore_exception, e)
        return True
    
    def create_topic(self, topic_name: str = "", num_partitions: int = 1,
                     replication_factor: int = 1,
                     ignore_exception: bool = False,
                     expire_time: int = 24 * 60 * 60 * 1000) -> bool:
        """Create one topic to Kafka Server

        Args:
            topic_name: the unique identifier of the topic
            num_partitions: the number of the partitions
                1. This parameter set how many partitions should be devided of the data from one topic.
                This data will be store in different cluster blocker.
                2. If the underlying message middleware supports partitioning (such as Kafka),
                  partitioning can be done based on this configuration;
                3. If the underlying message middleware do not support partitioning (such as Redis),
                ignore this parameter will be enough (one partition is enough). You can use Admin.is_support_partitions()
                to judge if the underlying message midddleware is support this feature or not.

            replication_factor: set how many copy of this topic data

                1. 该参数制定了在分布式集群部署的场景下，同一个主题的分区存在副本的数量，如果 replication_factor == 1
                   则表示主题下的所有分区都只有一个副本，一旦丢失不可回复；
                2. 如果底层的消息中间件支持数据副本，则可以依据该配置进行对应的设置；
                3. 如果底层的消息中间件不支持数据副本，则忽略该参数即可（即认定只有一个副本即可），可以通过
                   Admin.is_support_replication() 方法判定当前使用的小心中间件实现是否支持该特性；

            ignore_exception: 是否忽略可能会抛出的异常
            expire_time: 事件超时时间（单位：ms，默认：1day）

                1. 该参数指定了目标 Topic 中每个事件的有效期；
                2. 一旦一个事件的加入到 Topic 的时间超过了 expire_time，则cec不保证该事件
                   的持久性，cec应当在合适的时候删除超时的事件；
                3. 不强制要求超时的事件被立即删除，可以对超时的事件进行周期性的清理。

        Returns:
            bool: True if successful, False otherwise.

        Raises:
            TopicAlreadyExistsException: If topic already exists

        Examples:
            >>> admin = dispatch_admin("kafka://localhost:6379")
            >>> admin.create_topic("test_topic")
            True
        """
        return KafkaAdmin.static_create_topic(
            self._kafka_admin_client,
            topic_name, num_partitions, replication_factor, ignore_exception,
            expire_time
        )
    
    @staticmethod
    def static_del_topic(kafka_admin_client: AdminClient, topic_name: str,
                         ignore_exception: bool = False):
        """static method of deleting one topic
        
        this method of deleting one topic can be invoked by static method

        Args:
            kafka_admin_client (AdminClient): _description_
            topic_name (str): _description_
            ignore_exception (bool, optional): _description_. Defaults to False.

        Returns:
            _type_: _description_
        """
        try:
            res = kafka_admin_client.delete_topics([topic_name])
            res.get(topic_name).result()
        except KafkaException as ke:
            if ke.args[0].code() == KafkaError.UNKNOWN_TOPIC_OR_PART:
                return raise_if_not_ignore(ignore_exception,
                                           TopicNotExistsException(
                                               f"Someone else is creating or deleting "
                                               f"this topic."
                                           ))
            else:
                return raise_if_not_ignore(ignore_exception, ke)
        except Exception as e:
            return raise_if_not_ignore(ignore_exception, e)
        return True
    
    def del_topic(self, topic_name: str,
                  ignore_exception: bool = False) -> bool:
        """Delete one topic

        删除一个 Topic => 对应到 Kafka 应该是删除一个 Topic

        Args:
          topic_name: 主题名字（主题的唯一标识）
          ignore_exception: 是否忽略可能会抛出的异常

        Returns:
            bool: True if successful, False otherwise.

        Raises:
            TopicNotExistsException: If topic not exists

        Examples:
            >>> admin = dispatch_admin("kafka://localhost:6379")
            >>> admin.del_topic("test_topic")
            True
        """
        return KafkaAdmin.static_del_topic(self._kafka_admin_client,
                                           topic_name, ignore_exception)

    @staticmethod
    def static_is_topic_exist(kafka_admin_client: AdminClient,
                              topic_name: str) -> bool:
        class_meta = kafka_admin_client.list_topics(topic_name)
        return class_meta.topics.get(topic_name).error is None

    def is_topic_exist(self, topic_name: str) -> bool:
        """Judge whether one specific topic is exists

        判断 Topic 是否存在 => 对应到 Kafka 应该是判断是否存最对应topic
            1. 通过 list_topics 接口并指定 topic，判断返回的 Topic 是否有效进行判断

        Args:
            topic_name: 主题名字（主题的唯一标识）

        Returns:
            bool: True if topic exists, False otherwise.

        Examples:
            >>> admin = dispatch_admin("kafka://localhost:9092")
            >>> admin.is_topic_exist("test_topic")
            True
        """
        return KafkaAdmin.static_is_topic_exist(self._kafka_admin_client,
                                                topic_name)

    @staticmethod
    def static_get_topic_list(kafka_admin_client: AdminClient) -> [TopicMeta]:
        class_meta = kafka_admin_client.list_topics()
        res = []
        for topic in class_meta.topics.values():
            new_topic = TopicMeta(topic.topic)
            new_topic.error = topic.error
            for p_key, p_value in topic.partitions.items():
                new_topic.partitions[p_key] = PartitionMeta(p_value.id)
            res.append(new_topic)
        return res

    def get_topic_list(self) -> [TopicMeta]:
        """Get topic list

        获取 Topic 列表 => 对应到 Redis 应该是获取所有 Topic 的列表

        Args:

        Returns:
            [str]: The topic name list

        Examples:
            >>> admin = dispatch_admin("kafka://localhost:6379")
            >>> admin.get_topic_list()
            [TopicMeta(faeec676-60db-4418-a775-c5f1121d5331, 1)]
        """
        return KafkaAdmin.static_get_topic_list(self._kafka_admin_client)

    @staticmethod
    def static_create_consumer_group(kafka_admin_client: AdminClient,
                                     url: CecUrl, consumer_group_id: str,
                                     ignore_exception: bool = False
                                     ):

        if KafkaAdmin.static_is_consumer_group_exist(kafka_admin_client,
                                                     consumer_group_id):
            return raise_if_not_ignore(
                ignore_exception, ConsumerGroupAlreadyExistsException(
                    f"Consumer group {consumer_group_id} already exists."))

        _kafka_consumer_client = Consumer({
            'bootstrap.servers': url.netloc,
            "request.timeout.ms": 600000,
            'group.id': consumer_group_id,
            **url.params,
        })
        try:
            _kafka_consumer_client.subscribe(['__consumer_offsets'])
            _kafka_consumer_client.poll(0.1)
        except Exception as e:
            return raise_if_not_ignore(
                ignore_exception, e)
        return True

    def create_consumer_group(self, consumer_group_id: str,
                              ignore_exception: bool = False) -> bool:
        """Create one consumer group

        创建一个消费组
            1. __consumer_offsets 是 kafka 中用来传递消费者偏移量的特殊 Topic；
            2. 可以通过构建一个 Consumer，定语 __consumer_offsets 这个特殊主题，并且
               指定消费组 ID，则指定的消费组就会被正常创建。

        Args:
            consumer_group_id: 消费组ID，应当具有唯一性
            ignore_exception: 是否忽略可能会抛出的异常

        Returns:
            bool: True if successful, False otherwise.

        Raises:
            ConsumerGroupAlreadyExistsException: If consumer group already
            exists

        Examples:
            >>> admin = dispatch_admin("kafka://localhost:6379")
            >>> admin.create_consumer_group("test_group")
            True
        """
        res = KafkaAdmin.static_create_consumer_group(
            self._kafka_admin_client, self._current_cec_url, consumer_group_id,
            ignore_exception)
        return res

    def del_consumer_group(self, consumer_group_id: str,
                           ignore_exception: bool = False) -> bool:
        raise CecException(
            "Not implement del_consumer_group for current proto")

    @staticmethod
    def static_is_consumer_group_exist(
            kafka_admin_client: AdminClient,
            consumer_group_id: str) -> bool:
        return len(kafka_admin_client.list_groups(consumer_group_id)) > 0

    def is_consumer_group_exist(self, consumer_group_id: str) -> bool:
        return KafkaAdmin.static_is_consumer_group_exist(
            self._kafka_admin_client,
            consumer_group_id
        )

    @staticmethod
    def static_get_consumer_group_list(
            kafka_admin_client: AdminClient
    ) -> [ConsumerGroupMeta]:
        groups = kafka_admin_client.list_groups()
        res = []
        for group in groups:
            new_group = ConsumerGroupMeta(group.id)
            new_group.error = group.error
            for member in group.members:
                new_group.members.append(
                    ConsumerGroupMemberMeta(member.client_id))
            res.append(new_group)
        return res

    def get_consumer_group_list(self) -> [ConsumerGroupMeta]:
        return KafkaAdmin.static_get_consumer_group_list(
            self._kafka_admin_client
        )

    def get_consume_status(self, topic: str, consumer_group_id: str = "",
                           partition: int = 0) -> [ConsumeStatusItem]:
        """Get consumption info for specific <topic, consumer_group, partition>

        获取特定消费者组对某个主题下的特定分区的消费情况，应包含以下数据
        1. 最小ID（最小 offset）
        2. 最大ID（最大 offset）
        3. 分区中存储的事件总数（包括已消费的和未消费的）
        4. 最后一个当前消费组在该分区已确认的事件ID（最后一次消费者确认的事件的ID）
        5. 分区的消息堆积数量 LAG（已经提交到该分区，但是没有被当前消费者消费或确认的事件数量）

        Args:
            topic: 主题名字
            consumer_group_id: 消费组ID
                1. 如果 consumer_group_id 为空字符串或者None，则返回订阅了该主题的所有
                   消费组的消费情况；=> 此时 partition 参数无效（将获取所有分区的消费数据）
                2. 如果 consumer_group_id 为无效的组ID，则抛出异常；
                3. 如果 consumer_group_id 为有效的组ID，则只获取该消费组的消费情况。
            partition: 分区ID
                1. 如果 partition 指定有效非负整数 => 返回指定分区的消费情况
                2. 如果 partition 指定无效非负整数 => 抛出异常
                3. 如果 partition 指定负数 => 返回当前主题下所有分区的消费情况

        Raises:
            CecException

        References:
                https://github.com/confluentinc/confluent-kafka-python/blob/master/examples/list_offsets.py

        Returns:

        """

        def _inner_get_consume_status(
                c: ConfluentKafkaConsumer, group_id: str, tp: TopicPartition
        ):
            (lo, hi) = c.get_watermark_offsets(tp,
                                               timeout=10,
                                               cached=False)
            if tp.offset == confluent_kafka.OFFSET_INVALID:
                offset = "-"
            else:
                offset = f"{tp.partition}-{tp.offset - 1}"
            if hi < 0:
                lag = "no hwmark"  # Unlikely
            elif tp.offset < 0:
                lag = hi - lo
            else:
                lag = hi - tp.offset

            return ConsumeStatusItem(
                topic, group_id, tp.partition, min_id=f"{tp.partition}-{lo}",
                max_id=f"{tp.partition}-{hi - 1}", total_event_count=hi - lo, last_ack_id=offset, lag=lag
            )

        if consumer_group_id != "" and consumer_group_id is not None:
            # 获取指定消费组
            consumer = ConfluentKafkaConsumer({
                'bootstrap.servers': self._current_cec_url.netloc,
                'group.id': consumer_group_id
            })
            if partition >= 0:
                # 获取指定分区
                committed = consumer.committed(
                    [TopicPartition(topic, partition)], timeout=10)
                return [_inner_get_consume_status(
                    consumer, consumer_group_id, committed[0])]
            else:
                # 获取所有分区
                committed = consumer.committed([partition], timeout=10)
                return [
                    _inner_get_consume_status(consumer, consumer_group_id, tp)
                    for tp in committed]
        else:
            # 获取所有消费组的消费情况
            groups = self.get_consumer_group_list()
            res = []
            for group in groups:
                consumer = ConfluentKafkaConsumer({
                    'bootstrap.servers': self._current_cec_url.netloc,
                    'group.id': group.group_id
                })
                metadata = consumer.list_topics(topic, timeout=10)
                partitions = [confluent_kafka.TopicPartition(topic, p) for p in
                              metadata.topics[topic].partitions]
                committed = consumer.committed(partitions, timeout=10)
                for tp in committed:
                    if tp.offset == confluent_kafka.OFFSET_INVALID:
                        continue
                    else:
                        res.append(
                            _inner_get_consume_status(consumer, group.group_id,
                                                      tp))
            return res

    def get_event_list(self, topic: str, partition: int, offset: str,
                       count: int) -> [Event]:
        """ Get event list for specific <topic, partition>

        获取特定主题在指定分区下的消息列表
        1. offset 和 count 用于分页

        Args:
            topic: 主题名字
            partition: 分区ID
            offset: 偏移（希望读取在该 ID 之后的消息）
            count: 最大读取数量

        Returns:

        """
        consumer = ConfluentKafkaConsumer({
            'bootstrap.servers': self._current_cec_url.netloc,
            'group.id': uuid.uuid4()
        })
        tp = TopicPartition(topic, partition)
        if offset == '-':
            tp.offset = 0
        else:
            tp.offset = int(offset.split('-')[-1]) + 1

        consumer.assign([tp])
        kafka_messages = consumer.consume(count, timeout=10)
        messages = []
        for message in kafka_messages:
            event = Event(json.loads(message.value().decode('utf-8')),
                          f"{message.partition()}-{message.offset()}")
            event.put(KafkaAdmin._EVENT_KEY_KAFKA_CONSUMER_MESSAGE, message)
            messages.append(event)
        consumer.unassign()
        return messages

    def is_support_partitions(self) -> bool:
        return True

    def is_support_replication(self) -> bool:
        return True

    def connect_by_cec_url(self, url: CecUrl):
        """Connect to Kafka server by CecUrl

        Args:
          url(str): CecUrl
        """
        configs = {
            'bootstrap.servers': url.netloc,
            "request.timeout.ms": 600000,
            **url.params
        }
        """
        AdminClient is for Kafka Admin Object.
        This Object is init by a dictionary.
        confs = {
            'bootstrap.servers': url.netloc, # the location of the server
            "request.timeout.ms": 600000,
            'sasl.mechanisms': "", #optional, usually PLAIN
            'ssl.ca.location': "", #optional, the patch to certs
            'security.protocol': "", #optional, the security protocal, eg. SASL_SSL
            'sasl.username': "", # optional, provide it if Kafka server need it
            'sasl.password': "" # optional, provide it if Kafka server need it
        }
        """
        self._kafka_admin_client = AdminClient(conf=configs)
        self._current_url = url.__str__()

    def connect(self, url: str):
        """Connect to Kafka server by url

        Args:
          url(str): CecUrl
        """
        cec_url = CecUrl.parse(url)
        return self.connect_by_cec_url(cec_url)

    def disconnect(self):
        self._kafka_admin_client = None


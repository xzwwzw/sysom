-- 获取 KEY, FIELD
local field = ARGV[1]

-- 获取当前时间（以秒为单位）
local time = redis.call('TIME')
local current_time = tonumber(time[1]) + tonumber(time[2]) / 1000000

local expire_time_str = redis.call("HGET", KEYS[2], field)
local expire_time = -1
if expire_time_str ~= false then
    expire_time = tonumber(expire_time_str)
end

-- 1. 过期则删除并返回nil
-- 2. 不存在则返回nil
-- 3. 否则返回值
if (expire_time >= 0 and expire_time < current_time) then
    -- 删除过期的成员
    redis.call("HDEL", KEYS[1], field)
    redis.call("HDEL", KEYS[2], field)
    return nil
else
    local value = redis.call("HGET", KEYS[1], field)
    if value == false then
        return nil
    else
        return value
    end
end
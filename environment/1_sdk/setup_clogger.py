# -*- coding: utf-8 -*- #
"""
Time                2023/4/4 18:08
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                setup.py
Description:
"""
import setuptools

setuptools.setup(
    name="clogger",
    version="0.0.1",
    author="mingfeng(SunnyQjm)",
    author_email="mfeng@linux.alibaba.com",
    description="A common logger wrapper",
    url="",
    packages=setuptools.find_packages(),
    install_requires=[
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)

# -*- coding: utf-8 -*- #
"""
Time                2023/3/19 11:38
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                test_service_registry.py
Description:
"""

import uuid
import unittest
from cmg_base import dispatch_service_registry, CmgException, ServiceInstance


class MyTestCase(unittest.TestCase):
    """A test class to test service registry
    """

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        self.registry = dispatch_service_registry("redis://localhost:6379")
        self.test_service_name = "CMG_TEST_SERVICE_NAME"
        self.test_service_id = "CMG_TEST_SERVICE_ID"
        self.test_service_id_not_exists = "CMG_TEST_SERVICE_ID_NOT_EXISTS"

    def tearDown(self) -> None:
        pass

    def test_unregister_not_exists_services(self):
        not_exists_service_id = str(uuid.uuid4())
        self.assertRaises(CmgException, self.registry.unregister,
                          not_exists_service_id)

    def test_register(self):
        self.registry.register(ServiceInstance(
            service_name=self.test_service_name,
            service_id=self.test_service_id
        ))

    def test_unregister(self):
        self.registry.unregister(self.test_service_id)

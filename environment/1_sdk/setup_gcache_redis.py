import setuptools

setuptools.setup(
    name="gcache_redis",
    version="0.0.1",
    author="mingfeng(SunnyQjm)",
    author_email="mfeng@linux.alibaba.com",
    description="A redis-based GCache implement",
    url="",
    packages=setuptools.find_packages(),
    install_requires=[
        'redis'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)

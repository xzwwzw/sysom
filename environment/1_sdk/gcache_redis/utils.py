# -*- coding: utf-8 -*- #
"""
Time                2023/4/28 15:17
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                utils.py
Description:
"""
from gcache_base import GCacheUrl, GCacheException
from redis import Redis


def do_connect_by_gcache_url(cec_url: GCacheUrl) -> Redis:
    host_port = cec_url.netloc.split(":")
    if len(host_port) != 2:
        raise GCacheException(
            f"Not valid host:port => {host_port[0]}:{host_port[1]}")
    host, port = host_port[0], int(host_port[1])
    try:
        redis_client = Redis(host=host, port=port, db=0, decode_responses=True,
                             **cec_url.params)
    except ConnectionError as e:
        raise GCacheException(e)
    return redis_client

# -*- coding: utf-8 -*- #
"""
Time                2023/6/1 16:23
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                task.py
Description:
"""
from typing import List
from .filter import Filter, WildcardFilter, EqualFilter


class QueryTask:
    def __init__(self, metric_name: str, filters: List[Filter] = None):
        if filters is None:
            filters = []
        self.metric_name = metric_name
        self.filters = filters

    def append_wildcard_filter(self, label_name: str,
                               wildcard_rule: str) -> 'QueryTask':
        """Add wildcard filter

        Args:
            label_name(str): label or tag name, for example: cpu
            wildcard_rule: wildcard rule, for example: cpu*

        Returns:

        """
        self.filters.append(WildcardFilter(label_name, wildcard_rule))
        return self

    def append_equal_filter(self, label_name: str, value: str) -> 'QueryTask':
        """Add wildcard filter

        Args:
            label_name(str): label or tag name, for example: cpu
            value: value, for example: cpu0

        Returns:

        """
        self.filters.append(EqualFilter(label_name, value))
        return self

class RangeQueryTask(QueryTask):
    """
    Args:
        start_time(float): Start time (Unix timestamp)
        end_time(float): End time (Unix timestamp)
        step(int): Query resolution step width in duration format or float
                   number of seconds.
    """

    def __init__(self, metric_name: str, start_time: float, end_time: float,
                 step: int = 15, filters: List[Filter] = None):
        super().__init__(metric_name, filters)
        self.start_time = start_time
        self.end_time = end_time
        self.step = step

class InstantQueryTask(QueryTask):
    """
    Args:
        time(float): Instant time (Unix timestamp)
        aggregation(str):Built-in aggregation function of promql 
        (e.g. avg_over_time, rate, sum)
        interval(str): Range of time use to get a range vector(e.g. 5m)
    """ 
    def __init__(
        self,
        metric_name: str,
        time: float,
        aggregation: str = None,
        interval: str = None,
        aggregation_val: str = None,
        filters: List[Filter] = None,
        clause_label: List[str] = None,
    ):
        super().__init__(metric_name, filters)
        self.time = time
        self.aggregation = aggregation
        self.interval = interval
        self.clause = "by"
        self.clause_label = clause_label
        self.aggregation_val = aggregation_val
